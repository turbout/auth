<?php

namespace UnicaenAuthTest\Authentication;

use UnicaenAppTest\BaseServiceFactoryTest;

/**
 * Description of ModuleOptionsFactoryTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class ModuleOptionsFactoryTest extends BaseServiceFactoryTest
{
    protected $factoryClass = 'UnicaenAuth\Authentication\AuthenticationServiceFactory';
    protected $serviceClass = 'Zend\Authentication\AuthenticationService';

    public function testCanCreateService()
    {
        $storage = $this->createMock('UnicaenAuth\Authentication\Storage\Chain');
        $adapter = $this->createMock('ZfcUser\Authentication\Adapter\AdapterChain');

        $this->serviceManager->expects($this->exactly(2))
                ->method('get')
                ->will($this->returnValueMap([
                    ['UnicaenAuth\Authentication\Storage\Chain', $storage],
                    ['ZfcUser\Authentication\Adapter\AdapterChain', $adapter],
                ]));

        $service = $this->factory->__invoke($this->serviceManager, '');

        $this->assertInstanceOf($this->serviceClass, $service);
    }
}