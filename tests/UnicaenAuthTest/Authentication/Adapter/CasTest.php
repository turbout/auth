<?php

namespace UnicaenAuthTest\Authentication\Adapter;

use PHPUnit_Framework_TestCase;
use UnicaenApp\Exception\RuntimeException;
use UnicaenAuth\Authentication\Adapter\Cas;
use UnicaenAuth\Options\ModuleOptions;
use Zend\EventManager\EventManager;
use Zend\Router\Http\TreeRouteStack;
use Zend\Uri\Uri;
use ZfcUser\Authentication\Adapter\AdapterChainEvent;
use Zend\Authentication\Result;

/**
 * Description of CasTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class CasTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Cas
     */
    protected $adapter;

    /**
     * @var ModuleOptions
     */
    protected $moduleOptions;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->moduleOptions = $moduleOptions = new ModuleOptions([
            'cas' => [
                'connection' => [
                    'default' => [
                        'params' => [
                            'hostname' => 'cas.unicaen.fr',
                            'port' => 443,
                            'version' => "2.0",
                            'uri' => "",
                            'debug' => false,
                        ],
                    ],
                ],
            ],
        ]);

        $serviceManager = $this->createMock('Zend\ServiceManager\ServiceManager'/*, ['get']*/);
        $serviceManager->expects($this->any())
                       ->method('get')
                       ->will($this->returnCallback(function($serviceName) use ($moduleOptions) {
                           if ('zfcuser_module_options' === $serviceName) {
                               return new \ZfcUser\Options\ModuleOptions();
                           }
                           if ('unicaen-auth_module_options' === $serviceName) {
                               return $moduleOptions;
                           }
                           if ('router' === $serviceName) {
                               $router = new \Zend\Mvc\Router\Http\TreeRouteStack();
                               $router->setBaseUrl('/appli')->setRequestUri(new \Zend\Uri\Http('/request'));
                               return $router;
                           }
                           return null;
                       }));

        $this->adapter = new Cas();
        $this->adapter//->setServiceManager($serviceManager)
                      ->setEventManager(new EventManager());
    }

    public function getInvalidCasOptions()
    {
        return [
            [['other' => []]],
            [['connection' => []]],
            [['connection' => ['default'=> []]]],
            [['connection' => ['default'=> ['params' => []]]]],
        ];
    }

    /**
     * @dataProvider getInvalidCasOptions
     * @expectedException \Exception
     * @param array $config
     */
    public function testThrowsExceptionIfNoCasParamSpecified(array $config)
    {
        $this->moduleOptions->setCas($config);
        $this->adapter->setOptions($this->moduleOptions);
        $this->adapter->authenticate(new AdapterChainEvent());
    }

    public function testAuthenticateReturnsNullIfNoCasConfigSpecified()
    {
        $this->moduleOptions->setCas([]);
        $this->adapter->setOptions($this->moduleOptions);
        $result = $this->adapter->authenticate(new AdapterChainEvent());
        $this->assertNull($result);
    }

    public function testAuthenticatePopulatesEventWhenAuthenticationSucceeds()
    {
        /** @var \phpCAS|\PHPUnit_Framework_MockObject_MockObject $casClient */
        $casClient = $this->createMock(\phpCAS::class);
        $casClient->expects($this->once())
                  ->method('getUser')
                  ->will($this->returnValue($username = 'username'));

        $this->adapter->setCasClient($casClient);

        $event = new AdapterChainEvent();

        $this->adapter->setOptions($this->moduleOptions);
        $this->adapter->authenticate($event);

        $this->assertTrue($this->adapter->isSatisfied());
        $this->assertEquals(['is_satisfied' => true, 'identity' => $username], $this->adapter->getStorage()->read());

        $this->assertEquals("userAuthenticated", $event->getName());
        $this->assertEquals(Result::SUCCESS, $event->getCode());
        $this->assertEquals($username, $event->getIdentity());
    }

    public function testLogoutReturnsNullIfNoCasConfigSpecified()
    {
        $this->moduleOptions->setCas([]);
        $this->adapter->setOptions($this->moduleOptions);
        $result = $this->adapter->logout(new AdapterChainEvent());
        $this->assertNull($result);
    }

    public function testCanLogoutFromCasWithRedirectService()
    {
        /** @var \phpCAS|\PHPUnit_Framework_MockObject_MockObject $casClient */
        $casClient = $this->createMock(\phpCAS::class);
        $casClient->expects($this->once())
                  ->method('isAuthenticated')
                  ->will($this->returnValue(true));
        $casClient->expects($this->once())
                  ->method('logoutWithRedirectService');

        /** @var TreeRouteStack|\PHPUnit_Framework_MockObject_MockObject $router */
        $router = $this->createMock(TreeRouteStack::class);
        $router
            ->expects($this->once())
            ->method('getRequestUri')
            ->willReturn(new Uri());
        $router
            ->expects($this->once())
            ->method('getBaseUrl')
            ->willReturn('path');

        $this->adapter->setCasClient($casClient);

        $this->adapter->setOptions($this->moduleOptions);
        $this->adapter->setRouter($router);
        $this->adapter->logout(new AdapterChainEvent());
    }
}