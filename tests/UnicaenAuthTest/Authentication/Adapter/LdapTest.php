<?php

namespace UnicaenAuthTest\Authentication\Adapter;

use PHPUnit_Framework_TestCase;
use UnicaenApp\Mapper\Ldap\People;
use UnicaenAuth\Authentication\Adapter\Ldap;
use Zend\Authentication\Result;
use Zend\Authentication\Storage\StorageInterface;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManager;
use Zend\Http\Request;
use Zend\ServiceManager\ServiceManager;
use Zend\Stdlib\Parameters;
use ZfcUser\Authentication\Adapter\AdapterChainEvent;

/**
 * Description of LdapTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class LdapTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Ldap|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $adapter;

    /**
     * @var \Zend\Authentication\Adapter\Ldap|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $zendAuthLdapAdapter;

    /**
     * @var \UnicaenApp\Options\ModuleOptions
     */
    protected $appModuleOptions;

    /**
     * @var \UnicaenAuth\Options\ModuleOptions
     */
    protected $authModuleOptions;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->appModuleOptions = $appModuleOptions = new \UnicaenApp\Options\ModuleOptions([
            'ldap' => [
                'connection' => [
                    'default' => [
                        'params' => [
                            'host'                => 'host.domain.fr',
                            'username'            => "uid=xxxxxxxxx,ou=xxxxxxxxxx,dc=domain,dc=fr",
                            'password'            => "xxxxxxxxxxxx",
                            'baseDn'              => "ou=xxxxxxxxxxx,dc=domain,dc=fr",
                            'bindRequiresDn'      => true,
                            'accountFilterFormat' => "(&(objectClass=posixAccount)(supannAliasLogin=%s))",
                        ]
                    ]
                ]
            ],
        ]);
        $this->authModuleOptions = $authModuleOptions = new \UnicaenAuth\Options\ModuleOptions([
            'usurpation_allowed_usernames' => ['usurpateur'],
        ]);

        /** @var ServiceManager|\PHPUnit_Framework_MockObject_MockObject $serviceManager */
        $serviceManager = $this->createMock('Zend\ServiceManager\ServiceManager'/*, ['get']*/);
        $serviceManager->expects($this->any())
                       ->method('get')
                       ->will($this->returnCallback(function($serviceName) use ($authModuleOptions, $appModuleOptions) {
                           if ('zfcuser_module_options' === $serviceName) {
                               return new \ZfcUser\Options\ModuleOptions();
                           }
                           if ('unicaen-app_module_options' === $serviceName) {
                               return $appModuleOptions;
                           }
                           if ('unicaen-auth_module_options' === $serviceName) {
                               return $authModuleOptions;
                           }
                           return null;
                       }));

        $this->adapter = new Ldap();
        $this->adapter//->setServiceManager($serviceManager)
                      ->setEventManager(new EventManager());
    }

    public function testCanProvideDefaultLdapAuthAdapter()
    {
        $this->adapter->setAppModuleOptions($this->appModuleOptions);

        $adapter = $this->adapter->getLdapAuthAdapter();
        $this->assertInstanceOf('Zend\Authentication\Adapter\Ldap', $adapter);

        $appModuleLdapOptions = $this->appModuleOptions->getLdap();
        $connectionNames = array_keys($appModuleLdapOptions['connection']);
        $connectionParams = array_map(function($connection) { return $connection['params']; }, $appModuleLdapOptions['connection']);
        $this->assertEquals(array_combine($connectionNames, $connectionParams), $adapter->getOptions());
    }

    public function testAuthenticatingReturnsNullIfAlreadyStatisfied()
    {
        /** @var AdapterChainEvent|\PHPUnit_Framework_MockObject_MockObject $adapterChainEvent */
        $adapterChainEvent = $this->createMock(AdapterChainEvent::class);
        $adapterChainEvent
            ->expects($this->once())
            ->method('setIdentity')
            ->with('IDENTITY')
            ->willReturnSelf();
        $adapterChainEvent
            ->expects($this->once())
            ->method('setCode')
            ->with(Result::SUCCESS)
            ->willReturnSelf();

        /** @var EventInterface|\PHPUnit_Framework_MockObject_MockObject $event */
        $event = $this->createMock(EventInterface::class);
        $event
            ->expects($this->once())
            ->method('getTarget')
            ->willReturn($adapterChainEvent);

        /** @var StorageInterface|\PHPUnit_Framework_MockObject_MockObject $storage */
        $storage = $this->createMock(StorageInterface::class);
        $storage
            ->expects($this->exactly(2))
            ->method('read')
            ->willReturn(['is_satisfied' => true, 'identity' => 'IDENTITY']);

        $this->adapter->setStorage($storage);
        $this->assertNull($this->adapter->authenticate($event));
    }

    public function testUsurpationWithAllowedUsernameAndSuccessfulAuthentication()
    {
        /** @var \Zend\Authentication\Adapter\Ldap|\PHPUnit_Framework_MockObject_MockObject $zendAuthLdapAdapter */
        $this->zendAuthLdapAdapter = $this->createMock('Zend\Authentication\Adapter\Ldap');

        /** @var \Zend\Ldap\Ldap|\PHPUnit_Framework_MockObject_MockObject $ldap */
        $ldap = $this->createMock(\Zend\Ldap\Ldap::class);
        $ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->willReturn(true);

        /** @var \Zend\Authentication\Adapter\Ldap|\PHPUnit_Framework_MockObject_MockObject $ldapAuthAdapter */
        $this->zendAuthLdapAdapter->expects($this->once())
            ->method('getLdap')
            ->willReturn($ldap);

        $this->authModuleOptions->setUsurpationAllowedUsernames(['usurpateur']);
        $event = new AdapterChainEvent();
        $this->_authenticateWithUsurpation(Result::SUCCESS, $event);

        $this->assertTrue($this->adapter->isSatisfied());
        $this->assertEquals(['is_satisfied' => true, 'identity' => 'usurpe'], $this->adapter->getStorage()->read());

        $this->assertEquals("userAuthenticated", $event->getName());
        $this->assertEquals(Result::SUCCESS, $event->getCode());
        $this->assertEquals('usurpe', $event->getIdentity());
    }

    public function testUsurpationWithAllowedUsernameAndUnsuccessfulAuthentication()
    {
        /** @var \Zend\Authentication\Adapter\Ldap|\PHPUnit_Framework_MockObject_MockObject $zendAuthLdapAdapter */
        $this->zendAuthLdapAdapter = $this->createMock('Zend\Authentication\Adapter\Ldap');

        /** @var \Zend\Ldap\Ldap|\PHPUnit_Framework_MockObject_MockObject $ldap */
        $ldap = $this->createMock(\Zend\Ldap\Ldap::class);
        $ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->willReturn([]);

        /** @var \Zend\Authentication\Adapter\Ldap|\PHPUnit_Framework_MockObject_MockObject $ldapAuthAdapter */
        $this->zendAuthLdapAdapter->expects($this->once())
            ->method('getLdap')
            ->willReturn($ldap);

        $this->authModuleOptions->setUsurpationAllowedUsernames(['usurpateur']);
        $event = new AdapterChainEvent();
        $this->_authenticateWithUsurpation(Result::FAILURE, $event);

        $this->assertFalse($this->adapter->isSatisfied());
        $this->assertEquals(['is_satisfied' => false], $this->adapter->getStorage()->read());

        $this->assertNull($event->getName());
        $this->assertEquals(Result::FAILURE, $event->getCode());
        $this->assertNull($event->getIdentity());
        $this->assertFalse($event->propagationIsStopped());
    }

    public function testUsurpationWithAllowedButUnexistingUsername()
    {
        /** @var \Zend\Authentication\Adapter\Ldap|\PHPUnit_Framework_MockObject_MockObject $zendAuthLdapAdapter */
        $this->zendAuthLdapAdapter = $this->createMock('Zend\Authentication\Adapter\Ldap');

        /** @var \Zend\Ldap\Ldap|\PHPUnit_Framework_MockObject_MockObject $ldap */
        $ldap = $this->createMock(\Zend\Ldap\Ldap::class);
        $ldap
            ->expects($this->once())
            ->method('searchEntries')
            ->willReturn([]);

        /** @var \Zend\Authentication\Adapter\Ldap|\PHPUnit_Framework_MockObject_MockObject $ldapAuthAdapter */
        $this->zendAuthLdapAdapter->expects($this->once())
            ->method('getLdap')
            ->willReturn($ldap);

        $this->authModuleOptions->setUsurpationAllowedUsernames(['usurpateur']);
        $event = new AdapterChainEvent();
        $this->_authenticateWithUsurpation(Result::FAILURE, $event);

        $this->assertFalse($this->adapter->isSatisfied());
        $this->assertEquals(['is_satisfied' => false], $this->adapter->getStorage()->read());

        $this->assertNull($event->getName());
        $this->assertEquals(Result::FAILURE, $event->getCode());
        $this->assertNull($event->getIdentity());
        $this->assertFalse($event->propagationIsStopped());
    }

    public function testUsurpationWithNotAllowedUsernameAndSuccessfulAuthentication()
    {
        /** @var \Zend\Authentication\Adapter\Ldap|\PHPUnit_Framework_MockObject_MockObject $zendAuthLdapAdapter */
        $this->zendAuthLdapAdapter = $this->createMock('Zend\Authentication\Adapter\Ldap');

        $this->authModuleOptions->setUsurpationAllowedUsernames([]);
        $event = new AdapterChainEvent();
        $this->_authenticateWithUsurpation(Result::SUCCESS, $event);

        $this->assertTrue($this->adapter->isSatisfied());
        $this->assertEquals(['is_satisfied' => true, 'identity' => 'usurpateur=usurpe'], $this->adapter->getStorage()->read());

        $this->assertEquals("userAuthenticated", $event->getName());
        $this->assertEquals('usurpateur=usurpe', $event->getIdentity());
    }

    public function testUsurpationWithNotAllowedUsernameAndUnsuccessfulAuthentication()
    {
        /** @var \Zend\Authentication\Adapter\Ldap|\PHPUnit_Framework_MockObject_MockObject $zendAuthLdapAdapter */
        $this->zendAuthLdapAdapter = $this->createMock('Zend\Authentication\Adapter\Ldap');

        $this->authModuleOptions->setUsurpationAllowedUsernames([]);
        $event = new AdapterChainEvent();
        $this->_authenticateWithUsurpation(Result::FAILURE, $event);

        $this->assertFalse($this->adapter->isSatisfied());
        $this->assertEquals(['is_satisfied' => false], $this->adapter->getStorage()->read());

        $this->assertNull($event->getName());
        $this->assertEquals(Result::FAILURE, $event->getCode());
        $this->assertNull($event->getIdentity());
        $this->assertFalse($event->propagationIsStopped());
    }

    protected function _authenticateWithUsurpation($authenticationResultCode, AdapterChainEvent &$adapterChainEvent)
    {
        $usernameUsurpateur = 'usurpateur';
        $usernameUsurpe     = 'usurpe';
        $username           = $usernameUsurpateur . Ldap::USURPATION_USERNAMES_SEP . $usernameUsurpe;

        $this->zendAuthLdapAdapter->expects($this->once())
                        ->method('setUsername')
                        ->with($usernameUsurpateur)
                        ->will($this->returnSelf());
        $this->zendAuthLdapAdapter->expects($this->once())
                        ->method('setPassword')
                        ->will($this->returnSelf());
        $this->zendAuthLdapAdapter->expects($this->once())
                        ->method('authenticate')
                        ->will($this->returnValue(new Result($authenticationResultCode, $usernameUsurpateur)));
        $this->adapter->setLdapAuthAdapter($this->zendAuthLdapAdapter);

        $ldapPeopleMapper = $this->createMock(People::class);
        $ldapPeopleMapper
            ->expects($this->once())
            ->method('findOneByUsername')
            ->willReturn('not empty');

        $this->adapter->setLdapPeopleMapper($ldapPeopleMapper);

        $request = new Request();
        $request->setPost(new Parameters(['identity' => $username, 'credential' => "xxxxx"]));
        $adapterChainEvent->setRequest($request);

//        /** @var AdapterChainEvent|\PHPUnit_Framework_MockObject_MockObject $adapterChainEvent */
//        $adapterChainEvent = $this->createMock(AdapterChainEvent::class);
//        $adapterChainEvent
//            ->expects($this->once())
//            ->method('setIdentity')
//            ->with('IDENTITY')
//            ->willReturnSelf();
//        $adapterChainEvent
//            ->expects($this->once())
//            ->method('setCode')
//            ->with(Result::SUCCESS)
//            ->willReturnSelf();

        /** @var EventInterface|\PHPUnit_Framework_MockObject_MockObject $event */
        $event = $this->createMock(EventInterface::class);
        $event
            ->expects($this->once())
            ->method('getTarget')
            ->willReturn($adapterChainEvent);

        $this->adapter->setOptions($this->authModuleOptions);
        $this->adapter->authenticate($event);
    }
}