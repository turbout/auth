<?php

namespace UnicaenAuthTest\View\Helper;

use BjyAuthorize\Acl\Role;
use PHPUnit_Framework_MockObject_MockObject;
use UnicaenAppTest\View\Helper\TestAsset\ArrayTranslatorLoader;
use UnicaenAuth\Service\UserContext;
use UnicaenAuth\View\Helper\UserCurrent;
use UnicaenAuth\View\Helper\UserInfo;
use UnicaenAuth\View\Helper\UserProfile;
use UnicaenAuth\View\Helper\UserStatus;
use Zend\I18n\Translator\Translator;
use Zend\View\Helper\InlineScript;
use Zend\View\Renderer\PhpRenderer;

/**
 * Description of UserCurrentTest
 *
 * @property UserCurrent $helper Description
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class UserCurrentTest extends AbstractTest
{
    protected $helperClass = UserCurrent::class;

    /**
     * @var PhpRenderer|PHPUnit_Framework_MockObject_MockObject
     */
    protected $renderer;

    /**
     * @var UserContext|PHPUnit_Framework_MockObject_MockObject
     */
    protected $userContext;

    protected $userStatusHelper;
    protected $userProfileHelper;
    protected $userInfoHelper;
    protected $inlineScriptHelper;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->userStatusHelper   = $userStatusHelper   = $this->createMock(UserStatus::class/*, ['__invoke']*/);
        $this->userProfileHelper  = $userProfileHelper  = $this->createMock(UserProfile::class/*, ['__toString']*/);
        $this->userInfoHelper     = $userInfoHelper     = $this->createMock(UserInfo::class/*, ['__invoke']*/);
        $this->inlineScriptHelper = $inlineScriptHelper = $this->createMock(InlineScript::class/*, ['__invoke']*/);

        $this->userStatusHelper
                ->expects($this->any())
                ->method('__invoke')
                ->will($this->returnValue('User Status Helper Markup'));
        $this->userProfileHelper
                ->expects($this->any())
                ->method('__toString')
                ->will($this->returnValue('User Profile Helper Markup'));
        $this->userInfoHelper
                ->expects($this->any())
                ->method('__invoke')
                ->will($this->returnValue('User Info Helper Markup'));
        $this->inlineScriptHelper
                ->expects($this->any())
                ->method('__invoke')
                ->will($this->returnValue('InlineScript Helper Markup'));

        $this->renderer = $this->createMock(PhpRenderer::class/*, ['plugin']*/);
        $this->renderer->expects($this->any())
                       ->method('plugin')
                       ->will($this->returnCallback(function ($helper) use ($userStatusHelper, $userProfileHelper, $userInfoHelper, $inlineScriptHelper) {
                           if ('userstatus' === strtolower($helper)) {
                               return $userStatusHelper;
                           }
                           if ('userprofile' === strtolower($helper)) {
                               return $userProfileHelper;
                           }
                           if ('userinfo' === strtolower($helper)) {
                               return $userInfoHelper;
                           }
                           if ('inlinescript' === strtolower($helper)) {
                               return $inlineScriptHelper;
                           }
                           return null;
                       }));

        $this->userContext = $this->createMock(UserContext::class/*, ['hasIdentity', 'getIdentity']*/);

        $this->helper
            ->setUserContext($this->userContext)
            ->setTranslator(new Translator())
            ->setView($this->renderer);
    }

    public function testCanConstructWithUserContextService()
    {
        $helper = new UserCurrent($this->userContext);
        $this->assertSame($this->userContext, $helper->getUserContext());
    }

    public function testEntryPointReturnsSelfInstance()
    {
        $this->assertSame($this->helper, $this->helper->__invoke());
    }

    public function testEntryPointCanSetArgs()
    {
        $this->helper->__invoke($flag = true);
        $this->assertSame($flag, $this->helper->getAffectationFineSiDispo());
    }

    public function testCanRenderIfNoIdentityAvailable()
    {
        $this->userContext->expects($this->any())
                          ->method('getIdentity')
                          ->willReturn(false);

        $markup = $this->helper->__toString();
        $this->assertEquals($this->getExpected('user_current/logged-out.phtml'), $markup);

        // traduction
        $this->helper->setTranslator($this->_getTranslator());
        $markup = $this->helper->__toString();
        $this->assertEquals($this->getExpected('user_current/logged-out-translated.phtml'), $markup);
    }

    public function testCanRenderLogoutLinkIfIdentityAvailable()
    {
        $this->userContext
            ->expects($this->once())
            ->method('getSelectedIdentityRole')
            ->willReturn($role = new Role('role id'));
        $this->userContext->expects($this->once())
                          ->method('getIdentity')
                          ->willReturn($identity = 'Auth Service Identity');

        $markup = $this->helper->__toString();
        $this->assertEquals($this->getExpected('user_current/logged-in.phtml'), $markup);

        // traduction
        $this->helper->setTranslator($this->_getTranslator());
        $markup = $this->helper->__toString();
        $this->assertEquals($this->getExpected('user_current/logged-in-translated.phtml'), $markup);
    }

    /**
     * Returns translator
     *
     * @return Translator
     */
    protected function _getTranslator()
    {
        $loader = new ArrayTranslatorLoader();
        $loader->translations = [
            "Utilisateur connecté à l'application" => "Auth user",
            "Aucun"                                => "None",
        ];

        $translator = new Translator();
        $translator->getPluginManager()->setService('default', $loader);
        $translator->addTranslationFile('default', null);

        return $translator;
    }
}