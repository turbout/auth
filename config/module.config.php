<?php

use UnicaenAuth\Authentication\Storage\DbFactory;
use UnicaenAuth\Authentication\Storage\LdapFactory;
use UnicaenAuth\Authentication\Storage\ShibFactory;
use UnicaenAuth\Controller\AuthControllerFactory;
use UnicaenAuth\Controller\DroitsControllerFactory;
use UnicaenAuth\Controller\UtilisateurControllerFactory;
use UnicaenAuth\Form\Droits\RoleFormFactory;
use UnicaenAuth\Guard\PrivilegeControllerFactory;
use UnicaenAuth\Guard\PrivilegeRouteFactory;
use UnicaenAuth\ORM\Event\Listeners\HistoriqueListenerFactory;
use UnicaenAuth\Provider\Rule\PrivilegeRuleProviderFactory;
use UnicaenAuth\Service\ShibService;
use UnicaenAuth\Service\ShibServiceFactory;
use UnicaenAuth\Service\UserContextFactory;
use UnicaenAuth\Service\UserFactory;
use UnicaenAuth\Service\UserMapperFactory;
use UnicaenAuth\View\Helper\LdapConnectViewHelper;
use UnicaenAuth\View\Helper\LdapConnectViewHelperFactory;
use UnicaenAuth\View\Helper\LocalConnectViewHelper;
use UnicaenAuth\View\Helper\LocalConnectViewHelperFactory;
use UnicaenAuth\View\Helper\ShibConnectViewHelper;
use UnicaenAuth\View\Helper\ShibConnectViewHelperFactory;
use UnicaenAuth\View\Helper\UserConnection;
use UnicaenAuth\View\Helper\UserConnectionFactory;
use UnicaenAuth\View\Helper\UserCurrent;
use UnicaenAuth\View\Helper\UserCurrentFactory;
use UnicaenAuth\View\Helper\UserInfo;
use UnicaenAuth\View\Helper\UserInfoFactory;
use UnicaenAuth\View\Helper\UserProfile;
use UnicaenAuth\View\Helper\UserProfileFactory;
use UnicaenAuth\View\Helper\UserProfileSelect;
use UnicaenAuth\View\Helper\UserProfileSelectFactory;
use UnicaenAuth\View\Helper\UserProfileSelectRadioItem;
use UnicaenAuth\View\Helper\UserProfileSelectRadioItemFactory;
use UnicaenAuth\View\Helper\UserStatus;
use UnicaenAuth\View\Helper\UserStatusFactory;
use UnicaenAuth\View\Helper\UserUsurpationHelper;
use UnicaenAuth\View\Helper\UserUsurpationHelperFactory;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Proxy\LazyServiceFactory;

$settings = [

    /**
     * Configuration de l'authentification locale.
     */
    'local' => [
        /**
         * Possibilité ou non de s'authentifier à l'aide d'un compte local.
         */
        'enabled' => true,
    ],

    /**
     * Configuration de l'authentification LDAP.
     */
    'ldap' => [
        /**
         * Possibilité ou non de s'authentifier via l'annuaire LDAP.
         */
        'enabled' => true,
    ],

    /**
     * Fournisseurs d'identité.
     */
    'identity_providers'  => [
        300 => 'UnicaenAuth\Provider\Identity\Basic', // en 1er
        200 => 'UnicaenAuth\Provider\Identity\Db',    // en 2e
        100 => 'UnicaenAuth\Provider\Identity\Ldap',  // en 3e @deprecated
    ],

    /**
     * Attribut LDAP utilisé pour le username des utilisateurs
     */
    'ldap_username' => 'supannaliaslogin',
];

return [
    'zfcuser'         => [
        /**
         * Enable registration
         * Allows users to register through the website.
         * Accepted values: boolean true or false
         */
        'enable_registration'     => true,
        /**
         * Modes for authentication identity match
         * Specify the allowable identity modes, in the order they should be
         * checked by the Authentication plugin.
         * Default value: array containing 'email'
         * Accepted values: array containing one or more of: email, username
         */
        'auth_identity_fields'    => ['username', 'email'],
        /**
         * Login Redirect Route
         * Upon successful login the user will be redirected to the entered route
         * Default value: 'zfcuser'
         * Accepted values: A valid route name within your application
         */
        'login_redirect_route'    => 'home',
        /**
         * Logout Redirect Route
         * Upon logging out the user will be redirected to the enterd route
         * Default value: 'zfcuser/login'
         * Accepted values: A valid route name within your application
         */
        'logout_redirect_route'   => 'home',
        /**
         * Enable Username
         * Enables username field on the registration form, and allows users to log
         * in using their username OR email address. Default is false.
         * Accepted values: boolean true or false
         */
        'enable_username'         => false,
        /**
         * Enable Display Name
         * Enables a display name field on the registration form, which is persisted
         * in the database. Default value is false.
         * Accepted values: boolean true or false
         */
        'enable_display_name'     => true,
        /**
         * Authentication Adapters
         * Specify the adapters that will be used to try and authenticate the user
         * Default value: array containing 'ZfcUser\Authentication\Adapter\Db' with priority 100
         * Accepted values: array containing services that implement 'ZfcUser\Authentication\Adapter\ChainableAdapter'
         */
        'auth_adapters'           => [
            300 => 'UnicaenAuth\Authentication\Adapter\Ldap', // notifié en 1er
            200 => 'UnicaenAuth\Authentication\Adapter\Db',   //         ensuite (si échec d'authentification Ldap)
            100 => 'UnicaenAuth\Authentication\Adapter\Cas',  //         ensuite (si échec d'authentification Db)
        ],

        // telling ZfcUser to use our own class
        'user_entity_class'       => 'UnicaenAuth\Entity\Db\User',
        // telling ZfcUserDoctrineORM to skip the entities it defines
        'enable_default_entities' => false,
    ],
    'bjyauthorize'    => [

        /* role providers simply provide a list of roles that should be inserted
         * into the Zend\Acl instance. the module comes with two providers, one
         * to specify roles in a config file and one to load roles using a
         * Zend\Db adapter.
         */
        'role_providers'    => [
            /**
             * 2 rôles doivent systématiquement exister dans les ACL :
             * - le rôle par défaut 'guest', c'est le rôle de tout utilisateur non authentifié.
             * - le rôle 'user', c'est le rôle de tout utilisateur authentifié.
             */
            'UnicaenAuth\Provider\Role\Config'   => [
                'guest' => ['name' => "Non authentifié(e)", 'selectable' => false, 'children' => [
                    'user' => ['name' => "Authentifié(e)", 'selectable' => false],
                ]],
            ],
        ],

        // strategy service name for the strategy listener to be used when permission-related errors are detected
        //    'unauthorized_strategy' => 'BjyAuthorize\View\RedirectionStrategy',
        'unauthorized_strategy' => 'UnicaenAuth\View\RedirectionStrategy',

        /* Currently, only controller and route guards exist
         */
        'guards'                => [
            /* If this guard is specified here (i.e. it is enabled), it will block
             * access to all controllers and actions unless they are specified here.
             * You may omit the 'action' index to allow access to the entire controller
             */
            'BjyAuthorize\Guard\Controller'         => [
                ['controller' => 'index', 'action' => 'index', 'roles' => 'guest'],
                ['controller' => 'zfcuser', 'roles' => 'guest'],
                ['controller' => 'Application\Controller\Index', 'roles' => 'guest'],

                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'etab', 'roles' => 'guest'],
                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'apropos', 'roles' => 'guest'],
                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'contact', 'roles' => 'guest'],
                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'plan', 'roles' => 'guest'],
                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'mentions-legales', 'roles' => 'guest'],
                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'informatique-et-libertes', 'roles' => 'guest'],
                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'refresh-session', 'roles' => 'guest'],
                ['controller' => 'UnicaenAuth\Controller\Utilisateur', 'action' => 'selectionner-profil', 'roles' => 'guest'],

                ['controller' => 'UnicaenAuth\Controller\Auth', 'action' => 'shibboleth', 'roles' => 'guest'],
                ['controller' => 'UnicaenAuth\Controller\Auth', 'action' => 'requestPasswordReset', 'roles' => 'guest'],
                ['controller' => 'UnicaenAuth\Controller\Auth', 'action' => 'changePassword', 'roles' => 'guest'],
            ],
        ],
    ],
    'unicaen-auth'    => $settings,
    'doctrine'        => [
        'driver' => [
            // overriding zfc-user-doctrine-orm's config
            'zfcuser_entity'  => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => [
                    __DIR__ . '/../src/UnicaenAuth/Entity/Db',
                ],
            ],
            'orm_auth_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/UnicaenAuth/Entity/Db',
                ],
            ],
            'orm_default'     => [
                'class'   => 'Doctrine\ORM\Mapping\Driver\DriverChain',
                'drivers' => [
                    'UnicaenAuth\Entity\Db' => 'zfcuser_entity',
                    'UnicaenAuth\Entity\Db' => 'orm_auth_driver',
                ],
            ],
        ],
    ],
    'view_manager'    => [
        'template_map'        => [
            'error/403' => __DIR__ . '/../view/error/403.phtml',
        ],
        'template_path_stack' => [
            'unicaen-auth' => __DIR__ . '/../view',
        ],
    ],
    'translator'      => [
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ],
        ],
    ],
    'router'          => [
        'routes' => [
            'auth'     => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/auth',
                    'defaults' => [
                        'controller' => 'UnicaenAuth\Controller\Auth',
                    ],
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'shibboleth' => [
                        'type' => 'Literal',
                        'options' => [
                            'route'    => '/shibboleth',
                            'defaults' => [
                                'action'     => 'shibboleth',
                            ],
                        ],
                    ],
                    'requestPasswordReset' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/request-password-reset',
                            'defaults' => [
                                'action'     => 'requestPasswordReset',
                            ],
                        ],
                    ],
                    'changePassword' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/change-password/:token',
                            'defaults' => [
                                'action'     => 'changePassword',
                            ],
                        ],
                    ],
                ],
            ],
            'zfcuser'     => [
                'type'          => 'Literal',
                'priority'      => 1000,
                'options'       => [
                    'route'    => '/auth',
                    'defaults' => [
                        'controller' => 'zfcuser',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'login'    => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/connexion',
                            'defaults' => [
                                'controller' => 'zfcuser',
                                'action'     => 'login',
                            ],
                        ],
                    ],
                    'logout'   => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/deconnexion',
                            'defaults' => [
                                'controller' => 'zfcuser',
                                'action'     => 'logout',
                            ],
                        ],
                    ],
                    'register' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/creation-compte',
                            'defaults' => [
                                'controller' => 'zfcuser',
                                'action'     => 'register',
                            ],
                        ],
                    ],
                ],
            ],
            'utilisateur' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/utilisateur',
                    'defaults' => [
                        '__NAMESPACE__' => 'UnicaenAuth\Controller',
                        'controller'    => 'Utilisateur',
                        'action'        => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'default' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'       => '/:action[/:id]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[0-9]*',
                            ],
                            'defaults'    => [
                                'action' => 'index',
                            ],
                        ],
                    ],
                ],
            ],
            'droits'      => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/droits',
                    'defaults' => [
                        '__NAMESPACE__' => 'UnicaenAuth\Controller',
                        'controller'    => 'Droits',
                        'action'        => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'roles'      => [
                        'type'          => 'Segment',
                        'may_terminate' => true,
                        'options'       => [
                            'route'    => '/roles',
                            'defaults' => [
                                'action' => 'roles',
                            ],
                        ],
                        'child_routes'  => [
                            'edition'     => [
                                'type'          => 'Segment',
                                'may_terminate' => true,
                                'options'       => [
                                    'route'       => '/edition[/:role]',
                                    'constraints' => [
                                        'role' => '[0-9]*',
                                    ],
                                    'defaults'    => [
                                        'action' => 'role-edition',
                                    ],
                                ],
                            ],
                            'suppression' => [
                                'type'          => 'Segment',
                                'may_terminate' => true,
                                'options'       => [
                                    'route'       => '/suppression/:role',
                                    'constraints' => [
                                        'role' => '[0-9]*',
                                    ],
                                    'defaults'    => [
                                        'action' => 'role-suppression',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'privileges' => [
                        'type'          => 'Literal',
                        'may_terminate' => true,
                        'options'       => [
                            'route'    => '/privileges',
                            'defaults' => [
                                'action' => 'privileges',
                            ],
                        ],
                        'child_routes'  => [
                            'modifier' => [
                                'type'          => 'Segment',
                                'may_terminate' => true,
                                'options'       => [
                                    'route'    => '/modifier',
                                    'defaults' => [
                                        'action' => 'privileges-modifier',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    // All navigation-related configuration is collected in the 'navigation' key
    'navigation'      => [
        // The DefaultNavigationFactory we configured uses 'default' as the sitemap key
        'default' => [
            // And finally, here is where we define our page hierarchy
            'home' => [
                'pages' => [
                    'login'    => [
                        'label'   => _("Connexion"),
                        'route'   => 'zfcuser/login',
                        'visible' => false,
                    ],
                    'register' => [
                        'label'   => _("Enregistrement"),
                        'route'   => 'zfcuser/register',
                        'visible' => false,
                    ],
                ],
            ],
        ],
    ],
    //
    //( ! ) Warning: Declaration of
    // Application\Service\UserContextServiceAwareTrait::setUserContextService(Application\Service\UserContextService $userContextService)
    // should be compatible with
    // UnicaenAuth\Controller\UtilisateurController::setUserContextService(UnicaenAuth\Service\UserContext $userContextService)
    // in /var/www/sygal/module/Application/src/Application/Controller/UtilisateurController.php on line 34
    'service_manager' => [
        'aliases'            => [
            'Zend\Authentication\AuthenticationService' => 'zfcuser_auth_service',
            'UnicaenAuth\Privilege\PrivilegeProvider'   => 'UnicaenAuth\Service\Privilege',
            '\UnicaenAuth\Guard\PrivilegeController'    => 'UnicaenAuth\Guard\PrivilegeController',

            'unicaen-auth_user_service'               => 'UnicaenAuth\Service\User', // pour la compatibilité
            'authUserContext'                         => 'UnicaenAuth\Service\UserContext', // pour la compatibilité
            'AuthUserContext'                         => 'UnicaenAuth\Service\UserContext', // pour la compatibilité
        ],
        'invokables'         => [
            'UnicaenAuth\View\RedirectionStrategy'    => 'UnicaenAuth\View\RedirectionStrategy',
            'UnicaenAuth\Service\CategoriePrivilege'  => 'UnicaenAuth\Service\CategoriePrivilegeService',
        ],
        'abstract_factories' => [
            'UnicaenAuth\Authentication\Adapter\AbstractFactory',
        ],
        'factories'          => [
            'unicaen-auth_module_options'              => 'UnicaenAuth\Options\ModuleOptionsFactory',
            'zfcuser_auth_service'                     => 'UnicaenAuth\Authentication\AuthenticationServiceFactory',
            'UnicaenAuth\Authentication\Storage\Chain' => 'UnicaenAuth\Authentication\Storage\ChainServiceFactory',
            'UnicaenAuth\Provider\Identity\Chain'      => 'UnicaenAuth\Provider\Identity\ChainServiceFactory',
            'UnicaenAuth\Provider\Identity\Ldap'       => 'UnicaenAuth\Provider\Identity\LdapServiceFactory',
            'UnicaenAuth\Provider\Identity\Db'         => 'UnicaenAuth\Provider\Identity\DbServiceFactory',
            'UnicaenAuth\Provider\Identity\Basic'      => 'UnicaenAuth\Provider\Identity\BasicServiceFactory',
            'UnicaenAuth\Provider\Role\Config'         => 'UnicaenAuth\Provider\Role\ConfigServiceFactory',
            'UnicaenAuth\Provider\Role\DbRole'         => 'UnicaenAuth\Provider\Role\DbRoleServiceFactory',
            'UnicaenAuth\Provider\Role\Username'       => 'UnicaenAuth\Provider\Role\UsernameServiceFactory',
            'UnicaenAuth\Service\Role'                 => 'UnicaenAuth\Service\RoleServiceFactory',
            'UnicaenAuth\Service\Privilege'            => 'UnicaenAuth\Service\PrivilegeServiceFactory',
            'BjyAuthorize\Service\Authorize'           => 'UnicaenAuth\Service\AuthorizeServiceFactory', // substituion
            'zfcuser_redirect_callback'                => 'UnicaenAuth\Authentication\RedirectCallbackFactory', // substituion
            ShibService::class                         => ShibServiceFactory::class,
            'UnicaenAuth\Service\UserContext'          => UserContextFactory::class,
            'zfcuser_user_mapper'                      => UserMapperFactory::class,
            'MouchardCompleterAuth'        => 'UnicaenAuth\Mouchard\MouchardCompleterAuthFactory',
            'UnicaenAuth\Authentication\Storage\Db'   => DbFactory::class,
            'UnicaenAuth\Authentication\Storage\Ldap' => LdapFactory::class,
            'UnicaenAuth\Authentication\Storage\Shib' => ShibFactory::class,
            'UnicaenAuth\Service\User'                => UserFactory::class,
            'UnicaenAuth\Guard\PrivilegeController'   => PrivilegeControllerFactory::class,
            'UnicaenAuth\Guard\PrivilegeRoute'        => PrivilegeRouteFactory::class,
            'UnicaenAuth\Provider\Rule\PrivilegeRuleProvider' => PrivilegeRuleProviderFactory::class,

            'UnicaenApp\HistoriqueListener' => HistoriqueListenerFactory::class,
            'UnicaenAuth\HistoriqueListener' => HistoriqueListenerFactory::class,
            \UnicaenAuth\Event\EventManager::class => \UnicaenAuth\Event\EventManagerFactory::class
        ],
        'lazy_services' => [
            // Mapping services to their class names is required since the ServiceManager is not a declarative DIC.
            'class_map' => [
                'zfcuser_auth_service' => AuthenticationService::class,
            ],
        ],
        'delegators' => [
            'zfcuser_auth_service' => [
                LazyServiceFactory::class,
            ],
        ],
        'shared' => [
            'MouchardCompleterAuth'        => false,
        ],
        'initializers'       => [
            'UnicaenAuth\Service\UserAwareInitializer',
        ],
    ],

    'controllers'   => [
        'invokables' => [
        ],
        'factories' => [
            'UnicaenAuth\Controller\Auth'        => AuthControllerFactory::class,
            'UnicaenAuth\Controller\Utilisateur' => UtilisateurControllerFactory::class,
            'UnicaenAuth\Controller\Droits'      => DroitsControllerFactory::class,
        ],
    ],

    'form_elements' => [
        'invokables' => [
        ],
        'factories' => [
            'UnicaenAuth\Form\Droits\Role' => RoleFormFactory::class,
        ],
    ],

    'view_helpers'  => [
        'aliases' => [
            'userConnection'             => UserConnection::class,
            'userCurrent'                => UserCurrent::class,
            'userStatus'                 => UserStatus::class,
            'userProfile'                => UserProfile::class,
            'userInfo'                   => UserInfo::class,
            'userProfileSelect'          => UserProfileSelect::class,
            'userProfileSelectRadioItem' => UserProfileSelectRadioItem::class,
            'userUsurpation'             => UserUsurpationHelper::class,
            'localConnect'               => LocalConnectViewHelper::class,
            'ldapConnect'                => LdapConnectViewHelper::class,
            'shibConnect'                => ShibConnectViewHelper::class,
        ],
        'factories' => [
            UserConnection::class             => UserConnectionFactory::class,
            UserCurrent::class                => UserCurrentFactory::class,
            UserStatus::class                 => UserStatusFactory::class,
            UserProfile::class                => UserProfileFactory::class,
            UserInfo::class                   => UserInfoFactory::class,
            UserProfileSelect::class          => UserProfileSelectFactory::class,
            UserProfileSelectRadioItem::class => UserProfileSelectRadioItemFactory::class,
            UserUsurpationHelper::class       => UserUsurpationHelperFactory::class,
            LocalConnectViewHelper::class     => LocalConnectViewHelperFactory::class,
            LdapConnectViewHelper::class      => LdapConnectViewHelperFactory::class,
            ShibConnectViewHelper::class      => ShibConnectViewHelperFactory::class,
        ],
        'invokables' => [
            'appConnection' => 'UnicaenAuth\View\Helper\AppConnection',
        ],
    ],
];