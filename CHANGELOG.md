CHANGELOG
=========

3.0.0 (17/09/2019)
------------------
Première version officielle sous ZF3.


3.0.1 (18/09/2019)
------------------
- Corrections
  - Correction de ModuleOptionsFactory qui ne respectait son interface FactoryInterface.     
