<?php

namespace UnicaenAuth\Controller;

use Interop\Container\ContainerInterface;
use UnicaenAuth\Service\ShibService;
use UnicaenAuth\Service\User as UserService;
use Zend\Authentication\AuthenticationService;

class AuthControllerFactory
{
    /**
     * @param ContainerInterface $container
     * @return AuthController
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var ShibService $shibService */
        $shibService = $container->get(ShibService::class);

        /* @var $userService UserService */
        $userService = $container->get('unicaen-auth_user_service');

        /** @var AuthenticationService $authService */
        $authService = $container->get('zfcuser_auth_service');

        $controller = new AuthController();
        $controller->setShibService($shibService);
        $controller->setUserService($userService);
        $controller->setAuthenticationService($authService);

        return $controller;
    }
}