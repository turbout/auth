<?php

namespace UnicaenAuth\Form\Droits\Traits;

use RuntimeException;
use UnicaenAuth\Form\Droits\RoleForm;

/**
 * Description of RoleFormAwareTrait
 *
 * @author UnicaenCode
 */
trait RoleFormAwareTrait
{
    /**
     * @var RoleForm
     */
    private $formDroitsRole;



    /**
     * @param RoleForm $formDroitsRole
     *
     * @return self
     */
    public function setFormDroitsRole(RoleForm $formDroitsRole)
    {
        $this->formDroitsRole = $formDroitsRole;

        return $this;
    }



    /**
     * @return RoleForm
     * @throws RuntimeException
     */
    public function getFormDroitsRole()
    {
        return $this->formDroitsRole;
    }
}