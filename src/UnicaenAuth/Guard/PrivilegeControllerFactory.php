<?php

namespace UnicaenAuth\Guard;

use Interop\Container\ContainerInterface;
use UnicaenAuth\Provider\Privilege\PrivilegeProviderInterface;

class PrivilegeControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var PrivilegeProviderInterface $privilegeService */
        $privilegeProvider = $container->get('UnicaenAuth\Privilege\PrivilegeProvider');

        $rules = []; // NB: l'injection des vraies rules est faite par \BjyAuthorize\Service\BaseProvidersServiceFactory

        $instance = new PrivilegeController($rules, $container);
        $instance->setPrivilegeProvider($privilegeProvider);

        return $instance;
    }
}