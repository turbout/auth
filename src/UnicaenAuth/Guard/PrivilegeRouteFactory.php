<?php

namespace UnicaenAuth\Guard;

use Interop\Container\ContainerInterface;
use UnicaenAuth\Provider\Privilege\PrivilegeProviderInterface;

class PrivilegeRouteFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var PrivilegeProviderInterface $privilegeService */
        $privilegeProvider = $container->get('UnicaenAuth\Privilege\PrivilegeProvider');

        $rules = []; // NB: l'injection des vraies rules est faite par \BjyAuthorize\Service\BaseProvidersServiceFactory

        $instance = new PrivilegeRoute($rules, $container);
        $instance->setPrivilegeProvider($privilegeProvider);

        return $instance;
    }
}