<?php

namespace UnicaenAuth\Options;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Interop\Container\ContainerInterface;
use UnicaenApp\Exception\RuntimeException;

/**
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class ModuleOptionsFactory
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @return mixed
     */
    public function __invoke(ContainerInterface $container)
    {
        $config       = $container->get('Configuration');
        $moduleConfig = isset($config['unicaen-auth']) ? $config['unicaen-auth'] : [];
        $moduleConfig = array_merge($config['zfcuser'], $moduleConfig);

        $this->validateConfig($moduleConfig);

        return new ModuleOptions($moduleConfig);
    }

    /**
     * @param array $config
     */
    private function validateConfig(array $config)
    {
        $configKeyPath = ['unicaen-auth'];

        //
        // Config shibboleth.
        //
        $parentKey = 'shibboleth';
        if (array_key_exists($parentKey, $config)) {
            $shibConfig = $config[$parentKey];
            $configKeyPath[] = $parentKey;

            try {
                Assertion::keyExists($shibConfig, $k = 'logout_url');
            } catch (AssertionFailedException $e) {
                throw new RuntimeException(sprintf(
                    "La clé de configuration '%s.$k' est absente (inspirez-vous du fichier de config " .
                    "unicaen-auth.global.php.dist du module unicaen/auth si besoin)",
                    join('.', $configKeyPath)
                ));
            }

            array_pop($configKeyPath);
        }

        //
        // Autres.
        //

    }
}