<?php

namespace UnicaenAuth\Options\Traits;

use RuntimeException;
use UnicaenAuth\Options\ModuleOptions;

/**
 * Description of ModuleOptionsAwareTrait
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
trait ModuleOptionsAwareTrait
{
    /**
     * @var ModuleOptions
     */
    private $moduleOptions;



    /**
     * @param ModuleOptions $moduleOptions
     *
     * @return self
     */
    public function setModuleOptions(ModuleOptions $moduleOptions)
    {
        $this->moduleOptions = $moduleOptions;

        return $this;
    }



    /**
     * @return ModuleOptions
     * @throws RuntimeException
     */
    public function getModuleOptions()
    {
        return $this->moduleOptions;
    }
}