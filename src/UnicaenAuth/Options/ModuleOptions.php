<?php

namespace UnicaenAuth\Options;

/**
 * Classe encapsulant les options de fonctionnement du module.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class ModuleOptions extends \ZfcUser\Options\ModuleOptions
{
    /**
     * Paramètres concernant l'authentification locale.
     *
     * @var array
     */
    protected $local = [];

    /**
     * Paramètres concernant l'authentification LDAP.
     *
     * @var array
     */
    protected $ldap = [];

    /**
     * @var array
     */
    protected $usurpationAllowedUsernames = [];

    /**
     * @var bool
     */
    protected $saveLdapUserInDatabase = false;

    /**
     * @var string
     */
    protected $ldapUsername;

    /**
     * @var array
     */
    protected $shibboleth = [];

    /**
     * @var array
     */
    protected $cas = [];

    /**
     * @var string
     */
    protected $entityManagerName = 'doctrine.entitymanager.orm_default';

    /**
     * @return array
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * @param array $local
     * @return self
     */
    public function setLocal(array $local)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Retourne les paramètres concernant l'authentification LDAP.
     *
     * @return array
     */
    public function getLdap()
    {
        return $this->ldap;
    }

    /**
     * Spécifie les paramètres concernant l'authentification LDAP.
     *
     * @param array $ldap
     * @return self
     */
    public function setLdap(array $ldap)
    {
        $this->ldap = $ldap;

        return $this;
    }

    /**
     * set usernames allowed to make usurpation
     *
     * @param array $usurpationAllowedUsernames
     *
     * @return ModuleOptions
     */
    public function setUsurpationAllowedUsernames(array $usurpationAllowedUsernames = [])
    {
        $this->usurpationAllowedUsernames = $usurpationAllowedUsernames;

        return $this;
    }

    /**
     * get usernames allowed to make usurpation
     *
     * @return array
     */
    public function getUsurpationAllowedUsernames()
    {
        return $this->usurpationAllowedUsernames;
    }

    /**
     * Spécifie si l'utilisateur authentifié doit être enregistré dans la base
     * de données de l'appli
     *
     * @param bool $flag
     *
     * @return ModuleOptions
     */
    public function setSaveLdapUserInDatabase($flag = true)
    {
        $this->saveLdapUserInDatabase = (bool)$flag;

        return $this;
    }

    /**
     * Retourne la valeur du flag spécifiant si l'utilisateur authentifié doit être
     * enregistré dans la base de données de l'appli
     *
     * @return bool
     */
    public function getSaveLdapUserInDatabase()
    {
        return $this->saveLdapUserInDatabase;
    }

    /**
     * @return string
     */
    public function getLdapUsername()
    {
        return $this->ldapUsername;
    }

    /**
     * @param string $ldapUsername
     *
     * @return ModuleOptions
     */
    public function setLdapUsername($ldapUsername)
    {
        $this->ldapUsername = $ldapUsername;

        return $this;
    }

    /**
     * set cas connection params
     *
     * @param array $cas
     *
     * @return ModuleOptions
     */
    public function setCas(array $cas = [])
    {
        $this->cas = $cas;

        return $this;
    }

    /**
     * get cas connection params
     *
     * @return array
     */
    public function getCas()
    {
        return $this->cas;
    }

    /**
     * set shibboleth connection params
     *
     * @param array $shibboleth
     *
     * @return ModuleOptions
     */
    public function setShibboleth(array $shibboleth = [])
    {
        $this->shibboleth = $shibboleth;

        return $this;
    }

    /**
     * get shibboleth connection params
     *
     * @return array
     */
    public function getShibboleth()
    {
        return $this->shibboleth;
    }

    /**
     * @return string
     */
    public function getEntityManagerName()
    {
        return $this->entityManagerName;
    }

    /**
     * @param string $entityManagerName
     *
     * @return ModuleOptions
     */
    public function setEntityManagerName($entityManagerName)
    {
        $this->entityManagerName = $entityManagerName;

        return $this;
    }
}