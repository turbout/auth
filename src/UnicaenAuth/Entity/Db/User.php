<?php

namespace UnicaenAuth\Entity\Db;

use Doctrine\ORM\Mapping as ORM;

/**
 * User entity class.
 *
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 */
class User extends AbstractUser
{

}