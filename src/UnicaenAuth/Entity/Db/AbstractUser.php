<?php

namespace UnicaenAuth\Entity\Db;

use BjyAuthorize\Provider\Role\ProviderInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * User entity abstract mother class.
 *
 * @ORM\MappedSuperclass
 */
abstract class AbstractUser implements UserInterface, ProviderInterface
{
    const PASSWORD_LDAP = 'ldap';
    const PASSWORD_SHIB = 'shib';

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     */
    protected $username;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true,  length=255)
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(name="display_name", type="string", length=50, nullable=false)
     */
    protected $displayName;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     */
    protected $password;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    protected $state;

    /**
     * @var string
     * @ORM\Column(name="password_reset_token", type="string", length=256)
     */
    protected $passwordResetToken;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="UnicaenAuth\Entity\Db\Role")
     * @ORM\JoinTable(name="user_role_linker",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    protected $roles;

    /**
     * Initialies the roles variable.
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id.
     *
     * @param int $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = (int) $id;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username.
     *
     * @param string $username
     * @return self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email.
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get displayName.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set displayName.
     *
     * @param string $displayName
     * @return self
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password.
     *
     * @param string $password
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get state.
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set state.
     *
     * @param int $state
     * @return self
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getPasswordResetToken()
    {
        return $this->passwordResetToken;
    }

    /**
     * @param string $passwordResetToken
     * @return self
     */
    public function setPasswordResetToken($passwordResetToken = null)
    {
        $this->passwordResetToken = $passwordResetToken;

        return $this;
    }

    /**
     * Get role.
     *
     * @return Collection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add a role to the user.
     *
     * @param RoleInterface $role
     * @return self
     */
    public function addRole(RoleInterface $role)
    {
        $this->roles->add($role);

        return $this;
    }

    /**
     * Retourne true si cet utilisateur est local.
     *
     * Un utilisateur est local s'il ne résulte pas d'une authentification LDAP ou Shibboleth.
     * Son mot de passe est chiffré dans la table des utilisateurs.
     *
     * @return bool
     */
    public function isLocal()
    {
        return ! in_array($this->getPassword(), [
            AbstractUser::PASSWORD_LDAP,
            AbstractUser::PASSWORD_SHIB,
        ]);
    }

    /**
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getDisplayName();
    }
}