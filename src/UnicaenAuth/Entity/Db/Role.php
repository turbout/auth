<?php

namespace UnicaenAuth\Entity\Db;

use Doctrine\ORM\Mapping as ORM;

/**
 * Role entity class.
 *
 * @ORM\Entity
 * @ORM\Table(name="user_role")
 */
class Role extends AbstractRole
{

}