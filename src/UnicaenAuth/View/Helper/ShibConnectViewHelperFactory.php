<?php

namespace UnicaenAuth\View\Helper;

use Interop\Container\ContainerInterface;
use UnicaenAuth\Service\ShibService;

class ShibConnectViewHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return ShibConnectViewHelper
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var ShibService $shibService */
        $shibService = $container->get(ShibService::class);

        $helper = new ShibConnectViewHelper();
        $helper->setShibService($shibService);

        return $helper;
    }
}