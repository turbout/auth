<?php

namespace UnicaenAuth\View\Helper;

use Zend\Form\Form;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplatePathStack;

/**
 * Aide de vue dessinant le formulaire d'authentification locale,
 * si l'authentification locale est activée.
 *
 * @method PhpRenderer getView()
 * @author Unicaen
 */
class LocalConnectViewHelper extends AbstractHelper
{
    /**
     * @var bool
     */
    protected $enabled = true;

    /**
     * @var Form
     */
    protected $form;

    /**
     * @param bool $enabled
     * @return $this
     */
    public function setEnabled($enabled = true)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @param Form $form
     * @return $this
     */
    public function __invoke(Form $form)
    {
        $this->form = $form;

        $this->getView()->resolver()->attach(
            new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]])
        );

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if (! $this->enabled) {
            return '';
        }

        try {
            return $this->getView()->render("connect", [
                'title' => "Avec un compte local",
                'enabled' => $this->enabled,
                'form' => $this->form,
                'redirect' => null,
                'passwordReset' => true,
            ]);
        } catch (\Exception $e) {
            return '<p>' . $e->getMessage() . '</p><p>' . $e->getTraceAsString() . '</p>';
        }
    }
}