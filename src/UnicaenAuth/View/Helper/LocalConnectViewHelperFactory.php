<?php

namespace UnicaenAuth\View\Helper;

use Interop\Container\ContainerInterface;
use UnicaenAuth\Options\ModuleOptions;

class LocalConnectViewHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return LocalConnectViewHelper
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $config = $moduleOptions->getLocal();

        $enabled = isset($config['enabled']) && (bool) $config['enabled'];

        $helper = new LocalConnectViewHelper();
        $helper->setEnabled($enabled);

        return $helper;
    }
}