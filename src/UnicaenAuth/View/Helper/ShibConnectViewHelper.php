<?php

namespace UnicaenAuth\View\Helper;

use UnicaenAuth\Service\Traits\ShibServiceAwareTrait;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Renderer\PhpRenderer;

/**
 * Aide de vue dessinant le bouton de connexion via Shibboleth,
 * si l'authentification Shibboleth est activée.
 *
 * @method PhpRenderer getView()
 * @author Unicaen
 */
class ShibConnectViewHelper extends AbstractHelper
{
    use ShibServiceAwareTrait;

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            return $this->render();
        } catch (\Exception $e) {
            return '<p>' . $e->getMessage() . '</p><p>' . $e->getTraceAsString() . '</p>';
        }
    }

    /**
     * @return string
     */
    private function render()
    {
        if (! $this->shibService->isShibbolethEnabled()) {
            return '';
        }

        $shibUrl = $this->getView()->url('auth/shibboleth', [], ['query' => $this->getView()->queryParams()], true);

        return <<<EOS
<h3 class="connect-title">Via la fédération d'identité</h3> 
<a href="$shibUrl" class="btn btn-success btn-lg">Fédération d'identité Renater</a>
EOS;
    }
}