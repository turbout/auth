<?php

namespace UnicaenAuth\View\Helper;

use Interop\Container\ContainerInterface;
use UnicaenAuth\Options\ModuleOptions;

class LdapConnectViewHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return LdapConnectViewHelper
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $ldapArrayConfig = $moduleOptions->getLdap();

        $ldapEnabled = isset($ldapArrayConfig['enabled']) && (bool) $ldapArrayConfig['enabled'];

        $helper = new LdapConnectViewHelper();
        $helper->setEnabled($ldapEnabled);

        return $helper;
    }
}