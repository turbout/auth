<?php

namespace UnicaenAuth\View\Helper;

use UnicaenApp\Form\View\Helper\FormControlGroup;
use UnicaenAuth\Options\ModuleOptions;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;
use Zend\Form\View\Helper\Form as FormHelper;
use Zend\View\Renderer\PhpRenderer;

/**
 * Aide de vue permettant de saisir et valider le login de l'utilisateur dont on veut usurper l'identité.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class UserUsurpationHelper extends UserAbstract
{
    /**
     * @var PhpRenderer
     */
    protected $view;

    /**
     * @var ModuleOptions
     */
    protected $moduleOptions;

    /**
     * @var string
     */
    private $url;

    /**
     * @var bool
     */
    private $usurpationEnabled = false;

    /**
     * Point d'entrée.
     *
     * @return self
     */
    public function __invoke()
    {
        return $this;
    }
    
    /**
     * Retourne le code HTML généré par cette aide de vue.
     * 
     * @return string 
     */
    public function __toString()
    {
        if (! $this->usurpationEnabled) {
            return '';
        }

        /** @var FormHelper $formHelper */
        $formHelper = $this->view->plugin('form');
        /** @var FormControlGroup $formControlGroupHelper */
        $formControlGroupHelper = $this->view->plugin('formControlGroup');

        $form = new Form('user-usurpation-form');
        $form->setAttributes([
            'class' => 'user-usurpation-form',
            'action' => $this->url,
        ]);

        $input = new Text('identity');
        $input->setAttributes([
            'id' => 'user-usurpation-input',
            'placeholder' => "Identifiant utilisateur",
        ]);

        $submit = new Submit('submit');
        $submit->setValue("Usurper");
        $submit->setAttributes([
            'class' => 'user-usurpation-submit btn btn-info',
        ]);

        $html = '';
        $html .= $formHelper->openTag($form);
        $html .= "<div><strong>Usurpation d'identité :</strong></div>";
        $html .= $formControlGroupHelper->__invoke($input);
        $html .= $formControlGroupHelper->__invoke($submit);
        $html .= $formHelper->closeTag();

        $html .= <<<EOS
<script>
    $(".user-usurpation-form").submit(function() {
        $("body *").css('cursor', 'wait');
    });
    var input = $("#user-usurpation-input").on('input', function() {
        updateUsurpationSubmit();
    });
    function updateUsurpationSubmit() {
        $(".user-usurpation-submit").prop("disabled", input.val().length === 0);
    }
    updateUsurpationSubmit();
</script>
EOS;

        return $html;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @param ModuleOptions $moduleOptions
     * @return self
     */
    public function setModuleOptions(ModuleOptions $moduleOptions)
    {
        $this->moduleOptions = $moduleOptions;

        return $this;
    }

    /**
     * @param bool $usurpationEnabled
     * @return self
     */
    public function setUsurpationEnabled($usurpationEnabled = true)
    {
        $this->usurpationEnabled = $usurpationEnabled;

        return $this;
    }
}