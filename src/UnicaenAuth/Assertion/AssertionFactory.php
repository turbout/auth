<?php

namespace UnicaenAuth\Assertion;

use BjyAuthorize\Service\Authorize;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Application;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class AssertionFactory
 *
 * @package UnicaenAuth\Assertion
 */
class AssertionFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     *
     * @return UserContext
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /* @var $application Application */
        $application = $container->get('Application');
        $mvcEvent    = $application->getMvcEvent();

        /* @var $serviceAuthorize Authorize */
        $serviceAuthorize = $container->get('BjyAuthorize\Service\Authorize');

        /** @var UserContext $serviceUserContext */
        $serviceUserContext = $container->get('UnicaenAuth\Service\UserContext');

        /* @var $assertion AbstractAssertion */
        $assertion = new $requestedName;

        $assertion->setMvcEvent($mvcEvent);
        $assertion->setServiceAuthorize($serviceAuthorize);
        $assertion->setServiceUserContext($serviceUserContext);

        return $assertion;
    }
}
