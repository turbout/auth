<?php

namespace UnicaenAuth\Service;

use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;

class UserContextFactory
{
    /**
     * @param ContainerInterface $container
     * @return UserContext
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var AuthenticationService $authenticationService */
        $authenticationService = $container->get('Zend\Authentication\AuthenticationService');

        $service = new UserContext();
        $service->setAuthenticationService($authenticationService);

        //
        // NB: il serait souhaitable d'injecter l'identity provider dans le service UserContext en faisant :
        //
        //    $identityProvider = $container->get('BjyAuthorize\Provider\Identity\ProviderInterface');
        //    $service->setIdentityProvider($identityProvider);
        //
        // mais c'est impossible pour l'instant car il y a un cycle dans les dépendances entre services qui
        // provoque une boucle infinie.
        //

        return $service;
    }
}