<?php

namespace UnicaenAuth\Service\Traits;

use UnicaenAuth\Service\CategoriePrivilegeService;
use RuntimeException;

/**
 * Description of CategoriePrivilegeServiceAwareTrait
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
trait CategoriePrivilegeServiceAwareTrait
{
    /**
     * @var CategoriePrivilegeService
     */
    private $serviceCategoriePrivilege;



    /**
     * @param CategoriePrivilegeService $serviceCategoriePrivilege
     *
     * @return self
     */
    public function setServiceCategoriePrivilege(CategoriePrivilegeService $serviceCategoriePrivilege)
    {
        $this->serviceCategoriePrivilege = $serviceCategoriePrivilege;

        return $this;
    }



    /**
     * @return CategoriePrivilegeService
     * @throws RuntimeException
     */
    public function getServiceCategoriePrivilege()
    {
        return $this->serviceCategoriePrivilege;
    }
}