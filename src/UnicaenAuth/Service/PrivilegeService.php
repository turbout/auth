<?php

namespace UnicaenAuth\Service;

use Doctrine\ORM\EntityRepository;
use InvalidArgumentException;
use UnicaenAuth\Entity\Db\PrivilegeInterface;
use UnicaenAuth\Entity\Db\RoleInterface;
use UnicaenAuth\Provider\Privilege\PrivilegeProviderInterface;
use BjyAuthorize\Provider\Resource\ProviderInterface as ResourceProviderInterface;
use UnicaenAuth\Provider\Privilege\Privileges;

class PrivilegeService extends AbstractService
    implements PrivilegeProviderInterface, ResourceProviderInterface
{
    /**
     * @var string
     */
    protected $privilegeEntityClass;

    /**
     * @param string $privilegeEntityClass
     */
    public function setPrivilegeEntityClass($privilegeEntityClass)
    {
        if (! class_exists($privilegeEntityClass) || ! in_array(PrivilegeInterface::class, class_implements($privilegeEntityClass))) {
            throw new InvalidArgumentException("La classe spécifiée pour l'entité privilège doit implémenter " . PrivilegeInterface::class);
        }

        $this->privilegeEntityClass = $privilegeEntityClass;
    }

    /**
     * @var array
     */
    protected $privilegesRoles;

    /**
     * @return EntityRepository
     */
    public function getRepo()
    {
        return $this->getEntityManager()->getRepository($this->privilegeEntityClass);
    }

    /**
     * @return PrivilegeInterface[]
     */
    public function getList()
    {
        $qb = $this->getRepo()->createQueryBuilder('p')
            ->addSelect('c')
            ->join('p.categorie', 'c')
            ->addOrderBy('c.ordre')
            ->addOrderBy('p.ordre');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $id
     *
     * @return null|PrivilegeInterface
     */
    public function get($id)
    {
        return $this->getRepo()->findOneBy(['id' => $id]);
    }

    /**
     * @param PrivilegeInterface $privilege
     * @param RoleInterface      $role
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function addRole(PrivilegeInterface $privilege, RoleInterface $role)
    {
        $privilege->addRole($role);
        $this->getEntityManager()->flush();
    }

    /**
     * @param PrivilegeInterface $privilege
     * @param RoleInterface      $role
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function removeRole(PrivilegeInterface $privilege, RoleInterface $role)
    {
        $privilege->removeRole($role);
        $this->getEntityManager()->flush();
    }

    /**
     * Retourne un tableau à deux dimentions composé de chaînes de caractère UNIQUEMENT
     *
     * Format du tableau :
     * [
     *   'privilege_a' => ['role_1', ...],
     *   'privilege_b' => ['role_1', 'role_2', ...],
     * ]
     *
     * @return string[][]
     */
    public function getPrivilegesRoles()
    {
        if (null === $this->privilegesRoles) {
            $this->privilegesRoles = [];

            $qb = $this->getRepo()->createQueryBuilder('p')
                ->addSelect('c, r')
                ->join('p.categorie', 'c')
                ->join('p.role', 'r');
            $result = $qb->getQuery()->getResult();

            foreach ($result as $privilege) {
                /* @var $privilege PrivilegeInterface */
                $pr = [];
                foreach ($privilege->getRole() as $role) {
                    /* @var $role RoleInterface */
                    $pr[] = $role->getRoleId();
                }
                $this->privilegesRoles[$privilege->getFullCode()] = $pr;
            }
        }

        return $this->privilegesRoles;
    }

    /**
     * @return array
     */
    public function getResources()
    {
        $resources = [];
        $privileges = array_keys($this->getPrivilegesRoles());
        foreach ($privileges as $privilege) {
            $resources[] = Privileges::getResourceId($privilege);
        }

        return $resources;
    }
}