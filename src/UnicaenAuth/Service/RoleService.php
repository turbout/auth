<?php

namespace UnicaenAuth\Service;

use Doctrine\ORM\EntityRepository;
use InvalidArgumentException;
use UnicaenAuth\Entity\Db\RoleInterface;

/**
 * Class RoleService
 *
 * @package UnicaenAuth\Service
 * @author  Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class RoleService extends AbstractService
{
    /**
     * @var string
     */
    protected $roleEntityClass;

    /**
     * @param string $roleEntityClass
     */
    public function setRoleEntityClass($roleEntityClass)
    {
        if (! class_exists($roleEntityClass) || ! in_array(RoleInterface::class, class_implements($roleEntityClass))) {
            throw new InvalidArgumentException("La classe spécifiée pour l'entité rôle doit implémenter " . RoleInterface::class);
        }

        $this->roleEntityClass = $roleEntityClass;
    }

    /**
     * @return EntityRepository
     */
    public function getRepo()
    {
        return $this->getEntityManager()->getRepository($this->roleEntityClass);
    }

    /**
     * @return RoleInterface[]
     */
    public function getList()
    {
        $qb = $this->getRepo()->createQueryBuilder('r')->addOrderBy('r.roleId');
        $roles = $qb->getQuery()->getResult();

        return $roles;
    }

    /**
     * @param $id
     *
     * @return null|RoleInterface
     */
    public function get($id)
    {
        return $this->getRepo()->findOneBy(['id' => $id]);
    }

    /**
     * Sauvegarde le rôle en BDD
     *
     * @param RoleInterface $role
     * @return self
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(RoleInterface $role)
    {
        $this->getEntityManager()->persist($role);
        $this->getEntityManager()->flush($role);

        return $this;
    }

    /**
     * Supprime un rôle
     *
     * @param RoleInterface $role
     * @return $this
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(RoleInterface $role)
    {
        $this->getEntityManager()->remove($role);
        $this->getEntityManager()->flush($role);

        return $this;
    }

    /**
     * Nouvelle entité
     *
     * @return RoleInterface
     */
    public function newEntity()
    {
        return new $this->roleEntityClass;
    }
}