<?php

namespace UnicaenAuth\Service;

use Assert\Assertion;
use Assert\AssertionFailedException;
use InvalidArgumentException;
use UnicaenApp\Exception\LogicException;
use UnicaenApp\Exception\RuntimeException;
use UnicaenAuth\Entity\Shibboleth\ShibUser;
use Zend\Router\Http\TreeRouteStack;
use Zend\Session\Container;

/**
 * Shibboleth service.
 *
 * @author Unicaen
 */
class ShibService
{
    /**
     * @var \UnicaenAuth\Entity\Shibboleth\ShibUser
     */
    protected $authenticatedUser;

    /**
     * @var array
     */
    protected $shibbolethConfig = [];

    /**
     * @var array
     */
    protected $usurpationAllowedUsernames = [];

    /**
     * @return string
     */
    static public function apacheConfigSnippet()
    {
        $text = <<<EOS
<Location "/">
    AuthType Shibboleth
    ShibRequestSetting requireSession false
    Require shibboleth
</Location>
<Location "/auth/shibboleth">
        AuthType Shibboleth
        ShibRequestSetting requireSession true
        Require shibboleth
</Location>
EOS;
        return $text;
    }

    /**
     * @param array $shibbolethConfig
     */
    public function setShibbolethConfig(array $shibbolethConfig)
    {
        $this->shibbolethConfig = $shibbolethConfig;
    }

    /**
     * @param array $usurpationAllowedUsernames
     */
    public function setUsurpationAllowedUsernames(array $usurpationAllowedUsernames)
    {
        $this->usurpationAllowedUsernames = $usurpationAllowedUsernames;
    }

    /**
     * @return ShibUser|null
     */
    public function getAuthenticatedUser()
    {
        if (! $this->isShibbolethEnabled()) {
            return null;
        }

        if ($this->authenticatedUser === null) {

            // D'ABORD activation éventuelle de la simulation
            $this->handleSimulation();
            // ENSUITE activation éventuelle de l'usurpation
            $this->handleUsurpation();

            if (! $this->isAuthenticated()) {
                return null;
            }

            $this->authenticatedUser = $this->createShibUserFromServerArrayData();
        }

        return $this->authenticatedUser;
    }

    /**
     * @return bool
     */
    private function isAuthenticated()
    {
        return
            $this->getServerArrayVariable('REMOTE_USER') ||
            $this->getServerArrayVariable('Shib-Session-ID') ||
            $this->getServerArrayVariable('HTTP_SHIB_SESSION_ID');
    }

    /**
     * @return boolean
     */
    public function isShibbolethEnabled()
    {
        return array_key_exists('enable', $this->shibbolethConfig) && (bool) $this->shibbolethConfig['enable'];
    }

    /**
     * @return array
     */
    public function getShibbolethSimulate()
    {
        if (! array_key_exists('simulate', $this->shibbolethConfig) || ! is_array($this->shibbolethConfig['simulate'])) {
            return [];
        }

        return $this->shibbolethConfig['simulate'];
    }

    /**
     * @param string $attributeName
     * @return string
     */
    private function getShibbolethAliasFor($attributeName)
    {
        if (! array_key_exists('aliases', $this->shibbolethConfig) ||
            ! is_array($this->shibbolethConfig['aliases']) ||
            ! isset($this->shibbolethConfig['aliases'][$attributeName])) {
            return null;
        }

        return $this->shibbolethConfig['aliases'][$attributeName];
    }

    /**
     * Retourne les alias des attributs spécifiés.
     * Si un attribut n'a pas d'alias, c'est l'attribut lui-même qui est retourné.
     *
     * @param array $attributeNames
     * @return array
     */
    private function getAliasedShibbolethAttributes(array $attributeNames)
    {
        $aliasedAttributes = [];
        foreach ($attributeNames as $attributeName) {
            $alias = $this->getShibbolethAliasFor($attributeName);
            $aliasedAttributes[$attributeName] = $alias ?: $attributeName;
        }

        return $aliasedAttributes;
    }

    /**
     * Retourne true si la simulation d'un utilisateur authentifié via Shibboleth est en cours.
     *
     * @return bool
     */
    public function isSimulationActive()
    {
        if (array_key_exists('simulate', $this->shibbolethConfig) &&
            is_array($this->shibbolethConfig['simulate']) &&
            ! empty($this->shibbolethConfig['simulate'])) {
            return true;
        }

        return false;
    }

    /**
     * Retourne la liste des attributs requis.
     *
     * @return array
     */
    private function getShibbolethRequiredAttributes()
    {
        if (! array_key_exists('required_attributes', $this->shibbolethConfig)) {
            return [];
        }

        return (array)$this->shibbolethConfig['required_attributes'];
    }

    /**
     *
     */
    public function handleSimulation()
    {
        if (! $this->isSimulationActive()) {
            return;
        }

        try {
            $shibUser = $this->createShibUserFromSimulationData();
        } catch (AssertionFailedException $e) {
            throw new LogicException("Configuration erronée", null, $e);
        }

        $this->simulateAuthenticatedUser($shibUser);
    }

    /**
     * @return ShibUser
     * @throws AssertionFailedException
     */
    private function createShibUserFromSimulationData()
    {
        $data = $this->getShibbolethSimulate();

        $this->assertRequiredAttributesExistInData($data);

        $eppn = $this->getValueFromShibData('eppn', $data);
        $supannId = $this->getValueFromShibData('supannEmpId', $data) ?: $this->getValueFromShibData('supannEtuId', $data);
        $email = $this->getValueFromShibData('mail', $data);
        $displayName = $this->getValueFromShibData('displayName', $data);
        $givenName = $this->getValueFromShibData('givenName', $data);
        $surname = $this->getValueFromShibData('sn', $data);
        $civilite = $this->getValueFromShibData('supannCivilite', $data);

        Assertion::contains($eppn, '@', "L'eppn '" . $eppn . "' n'est pas de la forme 'id@domaine' attendue (ex: 'tartempion@unicaen.fr')");

        $shibUser = new ShibUser();
        $shibUser->setEppn($eppn);
        $shibUser->setId($supannId);
        $shibUser->setDisplayName($displayName);
        $shibUser->setEmail($email);
        $shibUser->setNom($surname);
        $shibUser->setPrenom($givenName);
        $shibUser->setCivilite($civilite);

        return $shibUser;
    }
    
    /**
     * Retourne true si les données stockées en session indiquent qu'une usurpation d'identité Shibboleth est en cours.
     *
     * @return bool
     */
    private function isUsurpationActive()
    {
        return $this->getSessionContainer()->offsetExists('fromShibUser');
    }

    /**
     * @param ShibUser $fromShibUser
     * @param ShibUser $toShibUser
     * @return self
     */
    public function activateUsurpation(ShibUser $fromShibUser, ShibUser $toShibUser)
    {
        // le login doit faire partie des usurpateurs autorisés
        if (! in_array($fromShibUser->getUsername(), $this->usurpationAllowedUsernames)) {
            throw new RuntimeException("Usurpation non autorisée");
        }

        $session = $this->getSessionContainer();
        $session->offsetSet('fromShibUser', $fromShibUser);
        $session->offsetSet('toShibUser', $toShibUser);

        return $this;
    }

    /**
     * Suppression des données stockées en session concernant l'usurpation d'identité Shibboleth.
     *
     * @return self
     */
    public function deactivateUsurpation()
    {
        $session = $this->getSessionContainer();
        $session->offsetUnset('fromShibUser');
        $session->offsetUnset('toShibUser');

        return $this;
    }

    /**
     * @return $this
     */
    public function handleUsurpation()
    {
        if (! $this->isUsurpationActive()) {
            return $this;
        }

        $session = $this->getSessionContainer();

        /** @var ShibUser|null $toShibUser */
        $toShibUser = $session->offsetGet('toShibUser');
        if ($toShibUser === null) {
            throw new RuntimeException("Anomalie: 'toShibUser' introuvable");
        }

        $this->simulateAuthenticatedUser($toShibUser, 'supannEmpId');

        return $this;
    }

    /**
     * @return Container
     */
    private function getSessionContainer()
    {
        return new Container(ShibService::class);
    }

    /**
     * @param array $data
     * @return array
     */
    private function getMissingRequiredAttributesFromData(array $data)
    {
        $requiredAttributes = $this->getShibbolethRequiredAttributes();
        $missingAttributes = [];

        foreach ($requiredAttributes as $requiredAttribute) {
            // un pipe permet d'exprimer un OU logique, ex: 'supannEmpId|supannEtuId'
            $attributes = array_map('trim', explode('|', $requiredAttribute));
            // attributs aliasés
            $attributes = $this->getAliasedShibbolethAttributes($attributes);

            $found = false;
            foreach (array_map('trim', $attributes) as $attribute) {
                if (isset($data[$attribute])) {
                    $found = true;
                }
            }
            if (!$found) {
                // attributs aliasés, dont l'un au moins est manquant, mise sous forme 'a|b'
                $missingAttributes[] = implode('|', $attributes);
            }
        }

        return $missingAttributes;
    }

    /**
     * @param array $data
     * @throws InvalidArgumentException
     */
    private function assertRequiredAttributesExistInData(array $data)
    {
        $missingAttributes = $this->getMissingRequiredAttributesFromData($data);

        if (!empty($missingAttributes)) {
            throw new InvalidArgumentException(
                "Les attributs suivants sont manquants : " . implode(', ', $missingAttributes));
        }
    }

    /**
     * Inscrit dans le tableau $_SERVER le nécessaire pour usurper l'identité d'un utilisateur
     * qui se serait authentifié via Shibboleth.
     *
     * @param ShibUser $shibUser Utilisateur dont on veut usurper l'identité.
     * @param string $keyForId Clé du tableau $_SERVER dans laquelle mettre l'id de l'utilsateur spécifié.
     *                         Ex: 'supannEmpId', 'supannEtuId'.
     */
    public function simulateAuthenticatedUser(ShibUser $shibUser, $keyForId = 'supannEmpId')
    {
        // 'REMOTE_USER' (notamment) est utilisé pour savoir si un utilisateur est authentifié ou non
        $this->setServerArrayVariable('REMOTE_USER', $shibUser->getEppn());

//        // on s'assure que tous les attributs obligatoires ont une valeur
//        foreach ($this->getShibbolethRequiredAttributes() as $requiredAttribute) {
//            // un pipe permet d'exprimer un OU logique, ex: 'supannEmpId|supannEtuId'
//            $attributes = array_map('trim', explode('|', $requiredAttribute));
//            foreach ($attributes as $attribute) {
//                $this->setServerArrayVariable($attribute, 'qqchose');
//            }
//        }

        // pour certains attributs, on veut une valeur sensée!
        $this->setServerArrayVariable('eppn', $shibUser->getEppn());
        $this->setServerArrayVariable($keyForId, $shibUser->getId());
        $this->setServerArrayVariable('displayName', $shibUser->getDisplayName());
        $this->setServerArrayVariable('mail', $shibUser->getEppn());
        $this->setServerArrayVariable('sn', $shibUser->getNom());
        $this->setServerArrayVariable('givenName', $shibUser->getPrenom());
        $this->setServerArrayVariable('supannCivilite', $shibUser->getCivilite());
    }

    /**
     * @return ShibUser
     */
    private function createShibUserFromServerArrayData()
    {
        try {
            $this->assertRequiredAttributesExistInData($_SERVER);
        } catch (InvalidArgumentException $e) {
            throw new RuntimeException('Des attributs Shibboleth obligatoires font défaut dans $_SERVER.', null, $e);
        }

        $eppn = $this->getServerArrayVariable('eppn');
        $id = $this->getServerArrayVariable('supannEtuId') ?: $this->getServerArrayVariable('supannEmpId');
        $mail = $this->getServerArrayVariable('mail');
        $displayName = $this->getServerArrayVariable('displayName');
        $surname = $this->getServerArrayVariable('sn') ?: $this->getServerArrayVariable('surname');
        $givenName = $this->getServerArrayVariable('givenName');
        $civilite = $this->getServerArrayVariable('supannCivilite');

        $shibUser = new ShibUser();
        // propriétés de UserInterface
        $shibUser->setId($id);
        $shibUser->setUsername($eppn);
        $shibUser->setDisplayName($displayName);
        $shibUser->setEmail($mail);
        $shibUser->setPassword(null);
        // autres propriétés
        $shibUser->setNom($surname);
        $shibUser->setPrenom($givenName);
        $shibUser->setCivilite($civilite);

        return $shibUser;
    }

    /**
     * Retourne l'URL de déconnexion Shibboleth.
     *
     * @param string $returnAbsoluteUrl Eventuelle URL *absolue* de retour après déconnexion
     * @return string
     */
    public function getLogoutUrl($returnAbsoluteUrl = null)
    {
        if ($this->getShibbolethSimulate()) {
            return '/';
        }
        if ($this->getAuthenticatedUser() === null) {
            return '/';
        }

        $logoutUrl = $this->shibbolethConfig['logout_url'];

        if ($returnAbsoluteUrl) {
            $logoutUrl .= urlencode($returnAbsoluteUrl);
        }

        return $logoutUrl;
    }

    /**
     * @param TreeRouteStack $router
     */
    public function reconfigureRoutesForShibAuth(TreeRouteStack $router)
    {
        $router->addRoutes([
            // remplace les routes existantes (cf. config du module)
            'zfcuser' => [
                'type'          => 'Literal',
                'priority'      => 1000,
                'options'       => [
                    'route'    => '/auth',
                    'defaults' => [
                        'controller' => 'zfcuser',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'login' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/connexion',
                            'defaults' => [
                                'controller' => 'zfcuser', // NB: lorsque l'auth Shibboleth est activée, la page propose
                                'action'     => 'login',   //     2 possibilités d'auth : LDAP et Shibboleth.
                            ],
                        ],
                    ],
                    'logout' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/:operation/shibboleth/',
                            'defaults' => [
                                'controller' => 'UnicaenAuth\Controller\Auth',
                                'action'     => 'shibboleth',
                                'operation'  => 'deconnexion'
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param string $name
     * @param array  $data
     * @return string
     */
    private function getValueFromShibData($name, array $data)
    {
        $key = $this->getShibbolethAliasFor($name) ?: $name;

        if (! array_key_exists($key, $data)) {
            return null;
        }

        return $data[$key];
    }

    /**
     * @param string $name
     * @param string $value
     */
    private function setServerArrayVariable($name, $value)
    {
        $key = $this->getShibbolethAliasFor($name) ?: $name;

        $_SERVER[$key] = $value;
    }

    /**
     * @param $name
     * @return string
     */
    private function getServerArrayVariable($name)
    {
        $key = $this->getShibbolethAliasFor($name) ?: $name;

        if (! array_key_exists($key, $_SERVER)) {
            return null;
        }

        return $_SERVER[$key];
    }
}