<?php

namespace UnicaenAuth\Service;

use Interop\Container\ContainerInterface;
use UnicaenAuth\Options\ModuleOptions;

class ShibServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');

        $service = new ShibService();
        $service->setShibbolethConfig($moduleOptions->getShibboleth());
        $service->setUsurpationAllowedUsernames($moduleOptions->getUsurpationAllowedUsernames());

        return $service;
    }
}