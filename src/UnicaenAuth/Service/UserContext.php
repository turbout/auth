<?php

namespace UnicaenAuth\Service;

use BjyAuthorize\Acl\Role;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Traits\SessionContainerTrait;
use UnicaenAuth\Acl\NamedRole;
use UnicaenAuth\Entity\Ldap\People;
use UnicaenAuth\Entity\Shibboleth\ShibUser;
use UnicaenAuth\Event\UserRoleSelectedEvent;
use UnicaenAuth\Formatter\RoleFormatter;
use UnicaenAuth\Provider\Identity\Chain;
use Zend\Authentication\AuthenticationService;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Zend\Permissions\Acl\Role\RoleInterface;
use ZfcUser\Entity\UserInterface;

/**
 * Service centralisant des méthodes utiles concernant l'utilisateur authentifié.
 *
 * @author Unicaen
 */
class UserContext extends AbstractService implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;
    use SessionContainerTrait;

    /**
     * @var mixed
     */
    protected $identity;

    /**
     * @var array
     */
    protected $identityRoles;

    /**
     * @var AuthenticationService
     */
    protected $authenticationService;

    /**
     * @param AuthenticationService $authenticationService
     */
    public function setAuthenticationService(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * Retourne l'utilisateur BDD courant
     *
     * @return UserInterface
     */
    public function getDbUser()
    {
        if (($identity = $this->getIdentity())) {
            if (isset($identity['db']) && $identity['db'] instanceof UserInterface) {
                return $identity['db'];
            }
        }

        return null;
    }

    /**
     * Retourne l'utilisateur LDAP courant
     *
     * @return People
     */
    public function getLdapUser()
    {
        if (($identity = $this->getIdentity())) {
            if (isset($identity['ldap']) && $identity['ldap'] instanceof People) {
                return $identity['ldap'];
            }
        }

        return null;
    }

    /**
     * Retourne l'éventuel utilisateur Shibboleth courant.
     *
     * @return ShibUser|null
     */
    public function getShibUser()
    {
        if (($identity = $this->getIdentity())) {
            if (isset($identity['shib']) && $identity['shib'] instanceof ShibUser) {
                return $identity['shib'];
            }
        }

        return null;
    }

    /**
     * Retourne les données d'identité correspondant à l'utilisateur courant.
     *
     * @return mixed
     */
    public function getIdentity()
    {
        if (null === $this->identity) {
            $authenticationService = $this->authenticationService;
            if ($authenticationService->hasIdentity()) {
                $this->identity = $authenticationService->getIdentity();
            }
        }

        return $this->identity;
    }

    /**
     * Retourne l'identifiant de connexion de l'utilisateur courant.
     *
     * @return string|null
     */
    public function getIdentityUsername()
    {
        if ($user = $this->getShibUser()) {
            return $user->getUsername();
        }
        if ($user = $this->getLdapUser()) {
            return $user->getUsername();
        }
        if ($user = $this->getDbUser()) {
            return $user->getUsername();
        }

        return null;
    }

    /**
     * @param string $roleId
     *
     * @return Role
     */
    public function getIdentityRole($roleId)
    {
        $roles = $this->getServiceAuthorize()->getRoles();
        if (isset($roles[$roleId])) {
            return $roles[$roleId];
        }

        return null;
    }

    /**
     * Retourne tous les rôles de l'utilisateur courant, pas seulement le rôle courant sélectionné.
     *
     * Les clés du tableau sont les ID de rôles, les valeurs sont les objets Role
     *
     * @return Role[]
     */
    public function getIdentityRoles()
    {
        if (null === $this->identityRoles) {
            $this->identityRoles = [];

            $roles            = $this->getServiceAuthorize()->getRoles();
            $identityProvider = $this->getIdentityProvider();
            if ($identityProvider instanceof Chain) {
                $identityRoles = $identityProvider->getAllIdentityRoles();
            } else {
                $identityRoles = $identityProvider->getIdentityRoles();
            }
            foreach ($identityRoles as $role) {
                if ($role instanceof RoleInterface) {
                    $this->identityRoles[$role->getRoleId()] = $role;
                } elseif (is_string($role) && isset($roles[$role])) {
                    $role = $roles[$role];
                    /** @var RoleInterface $role */
                    $this->identityRoles[$role->getRoleId()] = $role;
                }
            }
        }

        return $this->identityRoles;
    }

    /**
     * Retourne tous les rôles de l'utilisateur courant au format littéral.
     *
     * @return array
     * @see getIdentityRoles()
     */
    public function getIdentityRolesToString()
    {
        $f = new RoleFormatter();
        $rolesToStrings = [];

        foreach ($this->getIdentityRoles() as $identityRole) {
            $rolesToStrings[$identityRole->getRoleId()] = $f->format($identityRole);
        }

        return $rolesToStrings;
    }

    /**
     * Retourne parmi tous les rôles de l'utilisateur courant ceux qui peuvent être sélectionnés.
     *
     * @return array
     */
    public function getSelectableIdentityRoles()
    {
        $filter = function ($r) {
            return !($r instanceof NamedRole && !$r->getSelectable());
        };
        $roles  = array_filter($this->getIdentityRoles(), $filter);

        return $roles;
    }

    /**
     * Si un utilisateur est authentifié, retourne le rôle utilisateur sélectionné,
     * ou alors le premier sélectionnable si aucun n'a été sélectionné.
     *
     * NB: Si un rôle est spécifié en session comme devant être le prochain rôle sélectionné,
     * c'est lui qui est pris en compte.
     *
     * @return RoleInterface
     */
    public function getSelectedIdentityRole()
    {
        if ($this->getNextSelectedIdentityRole()) {
            $this->getSessionContainer()->selectedIdentityRole = $this->getNextSelectedIdentityRole();
        }

        if (null === $this->getSessionContainer()->selectedIdentityRole && $this->getIdentity()) {
            $roles = $this->getSelectableIdentityRoles();
            $this->setSelectedIdentityRole(reset($roles));
        }

        $roleId = $this->getSessionContainer()->selectedIdentityRole;
        if ($roleId) {
//            $roles = $this->getServiceAuthorize()->getRoles(); // Récupération de tous les rôles du provider
            $roles = $this->getIdentityRoles();
            if (isset($roles[$roleId])) {
                $role = $roles[$roleId];
            } else {
                $role = null;
            }

            if ($this->isRoleValid($role)) {
                return $role;
            }
        }

        return null;
    }

    /**
     * Retourne le rôle utilisateur sélectionné éventuel au format littéral.
     *
     * @return string
     * @see getSelectedIdentityRole()
     */
    public function getSelectedIdentityRoleToString()
    {
        $role = $this->getSelectedIdentityRole();

        if (! $role) {
            return null;
        }

        $f = new RoleFormatter();

        return $f->format($role);
    }

    /**
     * Mémorise en session le rôle spécifié comme étant le rôle courant de l'utilisateur.
     *
     * NB: seul l'id du rôle est mémorisé en session.
     *
     * @param RoleInterface|string $role
     *
     * @return \UnicaenAuth\Service\UserContext
     * @throws RuntimeException
     */
    public function setSelectedIdentityRole($role)
    {
        if ($role) {
            if (!$this->isRoleValid($role)) {
                throw new RuntimeException("Rôle spécifié invalide.");
            }
            if ($role instanceof RoleInterface) {
                $role = $role->getRoleId();
            }
            $this->getSessionContainer()->selectedIdentityRole = $role;
        } else {
            unset($this->getSessionContainer()->selectedIdentityRole);
        }

        $this->triggerUserRoleSelectedEvent(UserRoleSelectedEvent::POST_SELECTION, $role);

        return $this;
    }

    /**
     * Retourne l'éventuel rôle spécifié en session devant être le prochain rôle sélectionné.
     *
     * @return string|null
     */
    public function getNextSelectedIdentityRole()
    {
        return $this->getSessionContainer()->nextSelectedIdentityRole;
    }

    /**
     * Mémorise en session le rôle devant être le prochain rôle sélectionné.
     *
     * NB: seul l'id du rôle est mémorisé en session ; la durée de vie du stockage est de 1 requête seulement.
     *
     * @param RoleInterface|string $role
     *
     * @return \UnicaenAuth\Service\UserContext
     */
    public function setNextSelectedIdentityRole($role)
    {
        if ($role instanceof RoleInterface) {
            $role = $role->getRoleId();
        }

        if ($role) {
            $this->getSessionContainer()->nextSelectedIdentityRole = $role;
            $this->getSessionContainer()->setExpirationHops(1, 'nextSelectedIdentityRole');
        } else {
            unset($this->getSessionContainer()->nextSelectedIdentityRole);
        }

        $this->triggerUserRoleSelectedEvent(UserRoleSelectedEvent::POST_SELECTION, $role);

        return $this;
    }

    /**
     * Déclenche l'événement donnant à l'application l'opportunité de réagir à la sélection d'un rôle.
     *
     * @param string $name Ex: UserRoleSelectedEvent::POST_SELECTION
     * @param RoleInterface|string|null $role Rôle sélectionné
     */
    private function triggerUserRoleSelectedEvent($name, $role)
    {
        $event = new UserRoleSelectedEvent($name);
        $event
            ->setRole($role)
            ->setTarget($this);
        $this->getEventManager()->triggerEvent($event);
    }

    /**
     * Teste si le rôle spécifié fait partie des rôles disponibles.
     *
     * @param RoleInterface|string $role
     *
     * @return boolean
     */
    public function isRoleValid($role)
    {
        if ($role instanceof RoleInterface) {
            $role = $role->getRoleId();
        }

        foreach ($this->getIdentityRoles() as $r) {
            if ($r instanceof RoleInterface) {
                $r = $r->getRoleId();
            }
            if ($role === $r) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @return Chain
     */
    private function getIdentityProvider()
    {
        /* @var $identityProvider Chain */
        $identityProvider = $this->getServiceAuthorize()->getIdentityProvider();

        return $identityProvider;
    }
}