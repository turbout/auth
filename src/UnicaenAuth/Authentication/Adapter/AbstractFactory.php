<?php

namespace UnicaenAuth\Authentication\Adapter;

use Interop\Container\ContainerInterface;
use UnicaenApp\Exception\LogicException;
use UnicaenAuth\Options\ModuleOptions;
use UnicaenAuth\Service\User;
use UnicaenAuth\Event\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\Router\Http\TreeRouteStack;
use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use UnicaenApp\Mapper\Ldap\People as LdapPeopleMapper;

/**
 * Description of AbstractFactory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class AbstractFactory implements AbstractFactoryInterface
{
    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        return $this->canCreate($serviceLocator, $requestedName);
    }

    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        return $this->__invoke($serviceLocator, $requestedName);
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return strpos($requestedName, __NAMESPACE__) === 0 && class_exists($requestedName);
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        switch ($requestedName) {
            case __NAMESPACE__ . '\Ldap':
                $adapter = new Ldap();
                break;
            case __NAMESPACE__ . '\Db':
                $adapter = new Db();
                break;
            case __NAMESPACE__ . '\Cas':
                $adapter = new Cas();
                break;
            //
            // NB: pour faire simple, la stratégie de créer un adapter pour l'auth Shibboleth n'a pas été retenue.
            //
            // case __NAMESPACE__ . '\Shib':
            //     $adapter = new Shib();
            //     break;
            default:
                throw new LogicException("Service demandé inattendu : '$requestedName'!");
                break;
        }

        $this->injectDependencies($adapter, $container);

        if ($adapter instanceof EventManagerAwareInterface) {
            /** @var EventManager $eventManager */
            $eventManager = $container->get(EventManager::class);
            $adapter->setEventManager($eventManager);
            $userService = $container->get('unicaen-auth_user_service'); /* @var $userService \UnicaenAuth\Service\User */
            $eventManager->attach('userAuthenticated', [$userService, 'userAuthenticated'], 100);
            $eventManager->attach('clear', function() use ($adapter){
                $adapter->getStorage()->clear();
            });
        }

        return $adapter;
    }

    /**
     * @param Ldap|Db|Cas        $adapter
     * @param ContainerInterface $container
     */
    private function injectDependencies($adapter, ContainerInterface $container)
    {
        switch (true) {

            case $adapter instanceof Ldap:
                /** @var User $userService */
                $userService = $container->get('unicaen-auth_user_service');
                $adapter->setUserService($userService);

                /** @var LdapPeopleMapper $ldapPeopleMapper */
                $ldapPeopleMapper = $container->get('ldap_people_mapper');
                $adapter->setLdapPeopleMapper($ldapPeopleMapper);

                $options = array_merge(
                    $container->get('zfcuser_module_options')->toArray(),
                    $container->get('unicaen-auth_module_options')->toArray());
                $adapter->setOptions(new ModuleOptions($options));

                /** @var \UnicaenApp\Options\ModuleOptions $appModuleOptions */
                $appModuleOptions = $container->get('unicaen-app_module_options');
                $adapter->setAppModuleOptions($appModuleOptions);

                break;

            case $adapter instanceof Cas:
                /** @var User $userService */
                $userService = $container->get('unicaen-auth_user_service');
                $adapter->setUserService($userService);

                /** @var mixed $router */
                $router = $container->get('router');
                $adapter->setRouter($router);

                $options = array_merge(
                    $container->get('zfcuser_module_options')->toArray(),
                    $container->get('unicaen-auth_module_options')->toArray());
                $adapter->setOptions(new ModuleOptions($options));

                /** @var LdapPeopleMapper $ldapPeopleMapper */
                $ldapPeopleMapper = $container->get('ldap_people_mapper');
                $adapter->setLdapPeopleMapper($ldapPeopleMapper);

                break;

            default:
                break;
        }
    }
}