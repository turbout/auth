<?php

namespace UnicaenAuth\Authentication\Adapter;

use Interop\Container\ContainerInterface;
use UnicaenApp\ServiceManager\ServiceLocatorAwareInterface;
use UnicaenApp\ServiceManager\ServiceLocatorAwareTrait;
use UnicaenAuth\Options\ModuleOptions;
use Zend\Authentication\Storage\Session;
use Zend\EventManager\EventInterface;
use Zend\ServiceManager\Exception\ServiceNotFoundException;

/**
 * Adpater d'authentification à partir de la base de données.
 * 
 * Ajout par rapport à la classe mère : si aucune base de données ou table n'existe,
 * l'authentification ne plante pas (i.e. renvoit false).
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class Db extends \ZfcUser\Authentication\Adapter\Db implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Set service locator
     *
     * @param ContainerInterface $serviceLocator
     * @return self
     * @deprecated Abandonnez l'injection du service locator, svp
     */
    public function setServiceLocator(ContainerInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->serviceManager = $serviceLocator;

        return $this;
    }

    /**
     * Authentification.
     *
     * @param EventInterface $e
     * @return boolean
     */
    public function authenticate(EventInterface $e)
    {
        // NB: Dans la version 3.0.0 de zf-commons/zfc-user, cette méthode prend un EventInterface.
        // Mais dans la branche 3.x, c'est un AdapterChainEvent !
        // Si un jour c'est un AdapterChainEvent qui est attendu, plus besoin de faire $e->getTarget().
        if ($e->getTarget()->getIdentity()) {
            return true;
        }
        
        try {
            $result = parent::authenticate($e);
        }
        catch (ServiceNotFoundException $snfe) {
            return false;
        }

        return $result;
    }

    /**
     * @param \ZfcUser\Options\ModuleOptions $options
     * @return self
     */
    public function setOptions(\ZfcUser\Options\ModuleOptions $options)
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return ModuleOptions
     */
    public function getOptions()
    {
        if (!$this->options instanceof ModuleOptions) {
            $this->setOptions($this->getServiceManager()->get('unicaen-auth_module_options'));
        }
        return $this->options;
    }
}