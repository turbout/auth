<?php

namespace UnicaenAuth\Authentication\Storage;

use Interop\Container\ContainerInterface;
use UnicaenAuth\Service\ShibService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ShibFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var ShibService $shibService */
        $shibService = $container->get(ShibService::class);

        $storage = new Shib();
        $storage->setShibService($shibService);

        return $storage;
    }

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }
}