<?php

namespace UnicaenAuth\Authentication\Storage;

interface ChainableStorage
{
    /**
     * Returns the contents of storage
     *
     * Behavior is undefined when storage is empty.
     *
     * @param \UnicaenAuth\Authentication\Storage\ChainEvent $e
     */
    public function read(ChainEvent $e);

    /**
     * Writes $contents to storage
     *
     * @param \UnicaenAuth\Authentication\Storage\ChainEvent $e
     */
    public function write(ChainEvent $e);

    /**
     * Clears contents from storage
     *
     * @param \UnicaenAuth\Authentication\Storage\ChainEvent $e
     */
    public function clear(ChainEvent $e);
}