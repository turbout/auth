<?php

namespace UnicaenAuth\Authentication\Storage;

use Interop\Container\ContainerInterface;
use UnicaenAuth\Options\ModuleOptions;
use UnicaenAuth\Event\EventManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of ChainAuthenticationStorageServiceFactory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class ChainServiceFactory implements FactoryInterface
{
    private $mandatoryStorages = [
        200 => 'UnicaenAuth\Authentication\Storage\Ldap',
        100 => 'UnicaenAuth\Authentication\Storage\Db',
        75  => 'UnicaenAuth\Authentication\Storage\Shib',
    ];

    protected $storages = [];

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $chain = new Chain();

        /** @var EventManager $eventManager */
        $eventManager = $container->get(EventManager::class);

        $chain->setEventManager($eventManager);

        /** @var ModuleOptions $options */
        $options = $container->get('unicaen-auth_module_options');

        // retrait du fournisseur Ldap si l'auth LDAP est désactivée
        if (isset($options->getLdap()['enabled']) && ! $options->getLdap()['enabled']) {
            unset($this->mandatoryStorages[200]);
        }

        $storages = $this->mandatoryStorages + $this->storages;
        krsort($storages);

        foreach ($storages as $priority => $name) {
            $storage = $container->get($name);
            $chain->getEventManager()->attach('read', [$storage, 'read'], $priority);
            $chain->getEventManager()->attach('write', [$storage, 'write'], $priority);
            $chain->getEventManager()->attach('clear', [$storage, 'clear'], $priority);
        }

        return $chain;
    }
}