<?php

namespace UnicaenAuth\Authentication\Storage;

use Interop\Container\ContainerInterface;
use UnicaenApp\Mapper\Ldap\People as LdapPeopleMapper;
use UnicaenAuth\Options\ModuleOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LdapFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var LdapPeopleMapper $mapper */
        $mapper = $container->get('ldap_people_mapper');

        /** @var ModuleOptions $options */
        $options = $container->get('unicaen-auth_module_options');

        $storage = new Ldap();
        $storage->setMapper($mapper);
        $storage->setOptions($options);

        return $storage;
    }

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }
}