<?php

namespace UnicaenAuth\Authentication\Storage;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZfcUser\Mapper\UserInterface as UserMapper;

class DbFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var UserMapper $mapper */
        $mapper = $container->get('zfcuser_user_mapper');

        $storage = new Db();
        $storage->setMapper($mapper);

        return $storage;
    }

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }
}