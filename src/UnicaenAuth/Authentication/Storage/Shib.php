<?php

namespace UnicaenAuth\Authentication\Storage;

use UnicaenAuth\Entity\Shibboleth\ShibUser;
use UnicaenAuth\Service\Traits\ShibServiceAwareTrait;
use Zend\Authentication\Storage\Session;
use Zend\Authentication\Storage\StorageInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * Shibboleth authentication storage.
 *
 * @author Unicaen
 */
class Shib implements ChainableStorage
{
    use ShibServiceAwareTrait;

    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var ShibUser
     */
    protected $resolvedIdentity;

    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * Returns the contents of storage
     *
     * Behavior is undefined when storage is empty.
     *
     * @param ChainEvent $e
     * @return ShibUser
     * @throws \Zend\Authentication\Exception\ExceptionInterface
     */
    public function read(ChainEvent $e)
    {
        $shibUser = $this->getAuthenticatedUser();

        $e->addContents('shib', $shibUser);
        
        return $shibUser;
    }

    /**
     * @return null|ShibUser
     */
    private function getAuthenticatedUser()
    {
        if (null !== $this->resolvedIdentity) {
            return $this->resolvedIdentity;
        }

        $this->resolvedIdentity = $this->shibService->getAuthenticatedUser();

        return $this->resolvedIdentity;
    }

    /**
     * Writes $contents to storage
     *
     * @param ChainEvent $e
     * @throws \Zend\Authentication\Exception\ExceptionInterface
     */
    public function write(ChainEvent $e)
    {
        $contents = $e->getParam('contents');
        $this->resolvedIdentity = null;
        $this->getStorage()->write($contents);
    }

    /**
     * Clears contents from storage
     *
     * @param ChainEvent $e
     * @throws \Zend\Authentication\Exception\ExceptionInterface
     */
    public function clear(ChainEvent $e)
    {
        $this->resolvedIdentity = null;
        $this->getStorage()->clear();
    }

    /**
     * getStorage
     *
     * @return StorageInterface
     */
    public function getStorage()
    {
        if (null === $this->storage) {
            $this->setStorage(new Session());
        }

        return $this->storage;
    }

    /**
     * setStorage
     *
     * @param StorageInterface $storage
     * @return self
     */
    public function setStorage(StorageInterface $storage)
    {
        $this->storage = $storage;

        return $this;
    }
}
