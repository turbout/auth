<?php

namespace UnicaenAuth\Authentication\Storage;

use UnicaenApp\Mapper\Ldap\People as LdapPeopleMapper;
use UnicaenAuth\Entity\Ldap\People;
use UnicaenAuth\Options\ModuleOptions;
use Zend\Authentication\Storage\Session;
use Zend\Authentication\Storage\StorageInterface;

/**
 * Ldap authentication storage.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class Ldap implements ChainableStorage
{
    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var LdapPeopleMapper
     */
    protected $mapper;
    
    /**
     * @var ModuleOptions
     */
    protected $options;

    /**
     * @var People
     */
    protected $resolvedIdentity;

    /**
     * Returns the contents of storage
     *
     * Behavior is undefined when storage is empty.
     *
     * @param ChainEvent $e
     * @return People
     */
    public function read(ChainEvent $e)
    {
        $identity = $this->findIdentity();
        
        $e->addContents('ldap', $identity);
        
        return $identity;
    }

    /**
     * @return People|null
     */
    protected function findIdentity()
    {
        if (null !== $this->resolvedIdentity) {
            return $this->resolvedIdentity;
        }

        $identity = $this->getStorage()->read();

        if (is_scalar($identity)) {
            try {
                $identity = $this->getMapper()->findOneByUsername($identity);
            }
            catch (\Zend\Ldap\Exception\LdapException $exc) {
                $identity = null;
            }
            catch (\UnicaenApp\Exception $exc) {
                $identity = null;
            }
        }

        if ($identity) {
            $this->resolvedIdentity = new People($identity);
        } else {
            $this->resolvedIdentity = null;
        }

        return $this->resolvedIdentity;
    }

    /**
     * Writes $contents to storage
     *
     * @param ChainEvent $e
     */
    public function write(ChainEvent $e)
    {
        $contents = $e->getParam('contents');
        
        $this->resolvedIdentity = null;
        $this->getStorage()->write($contents);
    }

    /**
     * Clears contents from storage
     *
     * @param ChainEvent $e
     */
    public function clear(ChainEvent $e)
    {
        $this->resolvedIdentity = null;
        $this->getStorage()->clear();
    }

    /**
     * getStorage
     *
     * @return StorageInterface
     */
    public function getStorage()
    {
        if (null === $this->storage) {
            $this->setStorage(new Session());
        }
        return $this->storage;
    }

    /**
     * setStorage
     *
     * @param StorageInterface $storage
     * @access public
     * @return Ldap
     */
    public function setStorage(StorageInterface $storage)
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * getMapper
     *
     * @return LdapPeopleMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }

    /**
     * setMapper
     *
     * @param LdapPeopleMapper $mapper
     * @return Ldap
     */
    public function setMapper(LdapPeopleMapper $mapper = null)
    {
        $this->mapper = $mapper;
        return $this;
    }

    /**
     * @param ModuleOptions $options
     * @return Ldap
     */
    public function setOptions(ModuleOptions $options = null)
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return ModuleOptions
     */
    public function getOptions()
    {
        return $this->options;
    }
}
