<?php

namespace UnicaenAuth\Provider\Identity;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * LDAP identity provider factory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class LdapServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     * @deprecated
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config                 = $container->get('BjyAuthorize\Config');
        $user                   = $container->get('zfcuser_user_service');
        $simpleIdentityProvider = new Ldap($user->getAuthService());

        $simpleIdentityProvider->setDefaultRole($config['default_role']);
        $simpleIdentityProvider->setAuthenticatedRole($config['authenticated_role']);

        return $simpleIdentityProvider;
    }
}