<?php

namespace UnicaenAuth\Mouchard;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of MouchardCompleterAuthFactory
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class MouchardCompleterAuthFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator);
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $mouchardCompleterAuth = new MouchardCompleterAuth();

        $serviceUserContext = $container->get('UnicaenAuth\Service\UserContext');
        $mouchardCompleterAuth->setServiceUserContext($serviceUserContext);

        return $mouchardCompleterAuth;
    }
}