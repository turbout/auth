<?php

namespace UnicaenAuth;

use UnicaenAuth\Authentication\Adapter\Cas as CasAdapter;
use UnicaenAuth\Options\ModuleOptions;
use UnicaenAuth\Service\ShibService;
use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\ModuleManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\Navigation;
use ZfcUser\Form\Login;
use ZfcUser\Form\LoginFilter;

/**
 * Point d'entrée du module d'authentification Unicaen.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ServiceProviderInterface
{
    /**
     * @var ModuleOptions
     */
    private $options;

    /**
     * @return array
     * @see ConfigProviderInterface
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @return array
     * @see AutoloaderProviderInterface
     */
    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\ClassMapAutoloader' => [
                __DIR__ . '/autoload_classmap.php',
            ],
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    /**
     * This method is called once the MVC bootstrapping is complete,
     * after the "loadModule.post" event, once $application->bootstrap() is called.
     *
     * @param EventInterface $e
     *
     * @see BootstrapListenerInterface
     */
    public function onBootstrap(EventInterface $e)
    {
        /* @var \Zend\Mvc\MvcEvent $e */
        $application = $e->getApplication();
        /* @var $services \Zend\ServiceManager\ServiceManager */
        $services = $application->getServiceManager();

        /* @var $options ModuleOptions */
        $this->options = $services->get('unicaen-auth_module_options');

        $this->reconfigureRoutesForAuth($services);
    }

    /**
     * @param ServiceLocatorInterface $sl
     */
    private function reconfigureRoutesForAuth(ServiceLocatorInterface $sl)
    {
        /* @var $router \Zend\Router\Http\TreeRouteStack */
        $router = $sl->get('router');

        // si l'auth CAS est activée, modif de la route de connexion pour zapper le formulaire d'auth maison.
        $isCasEnable = (bool) $this->options->getCas();
        if ($isCasEnable && php_sapi_name() !== 'cli') {
            /** @var CasAdapter $casAdapter */
            $casAdapter = $sl->get('UnicaenAuth\Authentication\Adapter\Cas');
            $casAdapter->reconfigureRoutesForCasAuth($router);
        }

        // si l'auth Shibboleth est activée, modif de la route de déconnexion pour réaliser la déconnexion Shibboleth.
        $shibOptions = $this->options->getShibboleth();
        $isShibEnable = array_key_exists('enable', $shibOptions) && (bool) $shibOptions['enable'];
        if ($isShibEnable && php_sapi_name() !== 'cli') {
            /** @var ShibService $shibService */
            $shibService = $sl->get(ShibService::class);
            $shibService->reconfigureRoutesForShibAuth($router);
        }
    }

    /**
     * @return array
     * @see ServiceProviderInterface
     */
    public function getServiceConfig()
    {
        return [
            //========== repris du module zf-commons/zfc-user-doctrine-orm abandonné =========
            'aliases' => array(
                'zfcuser_doctrine_em' => 'Doctrine\ORM\EntityManager',
            ),
            //===========================================


            'factories' => [
                //========== repris du module zf-commons/zfc-user-doctrine-orm abandonné =========
                'zfcuser_module_options' => function ($sm) {
                    $config = $sm->get('Configuration');
                    return new Options\ModuleOptions(isset($config['zfcuser']) ? $config['zfcuser'] : array());
                },
                //===========================================

                // verrue pour forcer le label de l'identifiant qqsoit l'options 'auth_identity_fields'
                'zfcuser_login_form' => function ($sm) {
                    $options = $sm->get('zfcuser_module_options');
                    $form    = new Login(null, $options);
                    $form->setInputFilter(new LoginFilter($options));
                    $form->get('identity')->setLabel("Username");

                    return $form;
                },
            ],
        ];
    }
}