# Configuration

Il s'agit ici d'adapter certaines options de configuration des modules.

## Configuration globale

- Copier/coller/renommer le fichier config/unicaen-auth.global.php.dist du module vers config/autoload/unicaen-auth.global.php de votre projet.
- Adapter les informations suivantes à votre contexte (sans modifier le nom des clés)…

### Enregistrement automatique dans la base de l'appli de l'utilisateur authentifié via LDAP

Clé 'save_ldap_user_in_database' : flag indiquant si l'utilisateur authentifié avec succès via l'annuaire LDAP doit être enregistré/mis à jour dans la table des utilisateurs de l'appli (fonctionnalité du module ZfcUser).

*NB : Si vous activez cette fonctionnalité, vous devez spécifier dans la configuration locale du module (voir ci-dessous) les infos de connexion à la base de données d'authentification (“orm_auth”), soit spécifier dans la configuration locale d'un module quelconque les infos de connexion à la base de données principale de l'appli (“orm_default”).*

### Création de compte dans la base de l'appli par l'utilisateur lui-même

Clé 'enable_registration' : autorisation de la création d'un compte utilisateur par l'utilisateur lui-même dans la base de données de l'appli.

*NB : Si vous activez cette fonctionnalité, vous devez spécifier dans la configuration locale du module (voir ci-dessous) les infos de connexion à la base de données d'authentification (“orm_auth”), soit spécifier dans la configuration locale d'un module quelconque les infos de connexion à la base de données principale de l'appli (“orm_default”).*

### Système de gestion des privilèges

Le système de gestion des privilèges est activé par défaut. Il peut néanmoins être désactivé si

- il n'est pas utile à votre application
- si vous implémentez votre propre système de privilèges

La paramètre booléen unicaen-auth/enable_privileges en détermine l'activation.

### Désctivation d'autres parties d'UnicaenAuth

UnicaenAuth possède ses propres fournisseurs de rôles, d'identités (dans la configuration, rubrique respectives identity_provider et role_providers de bjyauthorize, pré-renseignées dans la configuration globale d'UnicaenAuth.

Tout comme le système de gestion des privilèges, il est possible de désactiver tout ou partie de ces systèmes pour les remplacer par les votres si nécessaire. Il suffit pour cela de commenter les lignes correspondantes dans le fichier de configuration global d'UnicaenAuth copié dans votre projet.

### Interface graphique de gestion (IHM)

Le système de gestion des privilèges d'UnicaenAuth est associé à une interface de gestion qui permet de :

- Gérer les rôles (ajout, modification et suppression de rôles) dynamiquement.
- Gérer l'association des rôles et des privilèges (les privilèges étant gérés directement en base de données car ces derniers sont liés au code source et ne sont pas dynamiques).

Un menu “Droits d'accès” est affiché par défaut dans votre barre de menu principale. Ceci peut bien entendu être modifié selon vos souhaits dans le fichier de configuration global d'UnicaenAuth placé dans votre projet.

## Configuration locale

- Copier/coller/renommer le fichier config/unicaen-auth.local.php.dist du module vers config/autoload/unicaen-auth.local.php de votre projet.
- Adapter les informations suivantes à votre contexte (sans modifier le nom des clés)…

### Authentification centralisée

Clé 'cas' : décommenter pour activer l'authentification CAS, commenter pour la désactiver, exemple :

unicaen-auth.local.php

        'cas' => array(
            'connection' => array(
                'default' => array(
                    'params' => array(
                        'hostname' => 'cas.unicaen.fr',
                        'port' => 443,
                        'version' => "2.0",
                        'uri' => "",
                        'debug' => false,
                    ),
                ),
            ),
        ),

### Usurpation d'identité

Clé 'usurpation_allowed_usernames' : liste des identifiants de connexion des utilisateurs (issus de l'annuaire LDAP) autorisés à se connecter à l'appli sous l'identité de n'importe quel utilisateur (issus de l'annuaire LDAP ou de la base de données d'authentification), exemple :

unicaen-auth.local.php

        'usurpation_allowed_usernames' => array('gauthierb'),

D'après cet exemple, l'utilisateur “gauthierb” est habilité à saisir dans le formulaire de connexion à l'appli l'identifiant “gauthierb=fernagut” et son mot de passe habituel pour se faire passer pour l'utilisateur “fernagut”.
